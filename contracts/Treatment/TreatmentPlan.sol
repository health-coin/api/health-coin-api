
pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;

/**
 * Er is geen check gedaan of een gebruiker een zorgverlener / zorgverzekeraar of deelnemer is.
 * Hiervoor moeten we kijken naar permissioning in parity
 * TODO: Soort van linking creeren om dit terug te koppelen aan de off-chain data.
 */
contract TreatmentPlan {

    event LogTreatmentPlanCaretaker(address treatment); // maybe listen for eventsq
    event LogTreatmentPlanCreated();
    
    enum State {
        DONE, // both signed for completion of contract.
        STARTED, // started (signed by participant & caretaker)
        PENDING, // awaiting for approval by participant
        SIGNED,
        END_MEASUREMENT_ADDED
    }
    struct Measurement {
        string unit;
        int128 quantity;
    }
    bytes32[] unitList;

    struct Treatment {
        bytes32 documentHash; // sha256 hash of off-chain treatmentplan
        address careTaker;
        address participant;
        uint256 plannedEndDate;
        uint256 startDate;
        int128 coinReward;
        State state;
        uint256 completionDate;
        bool redeemed;
    }

    Treatment private treatment;
    Measurement[] startMeasurements;
    Measurement[] goalMeasurements;
    Measurement[] endMeasurements;

    constructor(bytes32 documentHash, address participant, uint256 plannedEndDate, int128 coinReward, Measurement[] memory measurements) public {
        emit LogTreatmentPlanCreated();
        createTreatment(documentHash, msg.sender, participant, plannedEndDate, coinReward, measurements);
    }
    
    function requireToBeParticipant() view private {
        require(treatment.participant == msg.sender, "Het behandelplan is niet voor jou bedoelt om te tekenen");
    }
    function requireToBeCareTaker() view private {
        require(treatment.careTaker == msg.sender, "Je bent geen zorgverlener van dit behandelplan");

    }
    function requireTreatmentPendingOrEndMeasurementAdded() view private {
        require(treatment.state == State.PENDING || treatment.state == State.END_MEASUREMENT_ADDED, "Het behandelplan moet in afwachting staan of er moet een eindmeting zijn toegevoegd!");
    }
    function requireTreatmentPending() view private {
        require(treatment.state == State.PENDING, "Het behandelplan moet op in afwachting staan");
    }
    function requireTreatmentSigned() view private {
        require(treatment.state == State.SIGNED, "Het behandelplan moet op gesigneerd staan.");
    }
    function requireTreatmentToBeEndMeasured() view private {
        require(treatment.state == State.END_MEASUREMENT_ADDED, "Er moet een eind meting toegevoegd zijn aan het behandelplan");
    }
    function requireTreatmentStarted() view private {
        require(treatment.state == State.STARTED, "Het behandelplan moet gestart zijn.");
    }
    function requireTreatmentDone() view private {
        require(treatment.state == State.DONE, "Het behandelplan moet helemaal goedgekeurd zijn"); 
    }
    
    function requireToBePartOfContract() view private {
        require (msg.sender == treatment.careTaker || msg.sender == treatment.participant, "Je bent geen onderdeel van het contract");
    }
    function requireStartDateToBeSmallerThanEndDate() view private {
        require(treatment.startDate < treatment.plannedEndDate, "De start datum moet kleiner dan de einddatum");
    }
    function requireDocumentHashToBeValid(bytes32 givenDocumentHash) view private {
        require(treatment.documentHash == givenDocumentHash, "Je documentHash is niet correct! Hierdoor kun je niets opvragen of aanpassen in het contract");
    }
    
    function createTreatment(
        bytes32 documentHash,
        address careTaker,
        address participant,
        uint256 plannedEndDate,
        int128 coinReward,
        Measurement[] memory measurementTargets) private {
        emit LogTreatmentPlanCaretaker(careTaker);
        //requireStartDateToBeSmallerThanEndDate();

        treatment = Treatment(documentHash, careTaker, participant, plannedEndDate, now, coinReward, State.PENDING, 0, false);
        addGoalMeasurement(documentHash, measurementTargets);

    }

    function addGoalMeasurement(bytes32 documentHash, Measurement[] memory test) private {
        requireToBeCareTaker();
        requireDocumentHashToBeValid(documentHash);
        for(uint i = 0; i < test.length; i++) {
            goalMeasurements.push(test[i]);

        }
    }
        
    function addFirstMeasurement(bytes32 documentHash, Measurement[] memory measurements) public {
        addGuidedMeasurement(documentHash, true, measurements);
        treatment.state = State.STARTED;
    }

    function addEndMeasurement(bytes32 documentHash, Measurement[] memory measurements) public {
        addGuidedMeasurement(documentHash, false, measurements);
        treatment.completionDate = now; // update final date.
        treatment.state = State.END_MEASUREMENT_ADDED;
    }
    
    function signTreatmentAsParticipant(bytes32 documentHash, State state) private {
        requireToBeParticipant();
        requireTreatmentPendingOrEndMeasurementAdded();
        requireDocumentHashToBeValid(documentHash);
        treatment.state = state;
    }

    function signTreatmentToBeDoneAsParticipant(bytes32 documentHash) public {
        signTreatmentAsParticipant(documentHash, State.DONE);
        treatment.redeemed = false;
    }
   
    function signTreatmentToStartAsParticipant(bytes32 documentHash) public {
        signTreatmentAsParticipant(documentHash, State.SIGNED);
    }

    function addGuidedMeasurement(bytes32 documentHash, bool isFirst, Measurement[] memory measurements) private {
        // only the caretaker which started this contract can add a guided measurement
        requireToBeCareTaker();
        requireDocumentHashToBeValid(documentHash);
        if(isFirst) {
            requireTreatmentSigned();
            for(uint i = 0; i < measurements.length; i++) {
                startMeasurements.push(measurements[i]);

            }
        } else {
            requireTreatmentStarted();
            for(uint i = 0; i < measurements.length; i++) {
                endMeasurements.push(measurements[i]);

            }
        }
    }


    
    /**
     * TODO: Protect with document hash
     */
    function getTreatment(bytes32 documentHash) public view returns (Treatment memory test) {
        requireToBePartOfContract();
        requireDocumentHashToBeValid(documentHash);
        return treatment;
    }

    function getStartMeasurement(bytes32 documentHash) public view returns(Measurement[] memory) {
        requireToBePartOfContract();
        requireDocumentHashToBeValid(documentHash);
        return startMeasurements;
    }
    
    function getGoalMeasurement(bytes32 documentHash) public view returns (Measurement[] memory) {
        requireToBePartOfContract();
        requireDocumentHashToBeValid(documentHash);
        return goalMeasurements;
    }
    
    function getEndMeasurement(bytes32 documentHash) public view returns (Measurement[] memory) {
        requireToBePartOfContract();
        requireDocumentHashToBeValid(documentHash);
        return endMeasurements;
    }
}