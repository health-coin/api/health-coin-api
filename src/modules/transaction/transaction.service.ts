import * as FS from 'fs';
import mainConfig from '../../../config/main.config';
import { web3 } from '../../index';
import { RewardTransactionQuery, TransactionRequest } from './typings';

const healthCoinABI = JSON.parse(FS.readFileSync('./build/contracts/HealthCoin.json').toString());

export const getBalance = async (): Promise<number> => {
  return (await getHealthCoinContract().methods.getTotal().call()).toString();
};

export const getUserBalance = async (id: string): Promise<number> => {
  return (await getHealthCoinContract().methods.balanceOf(id).call({})).toString();
};

export const rewardHealthcoins = async (query: RewardTransactionQuery) => {
  const { amount } = query;

  return new Promise((resolve, reject) => {
    getHealthCoinContract().methods.mintToken(query.to, amount)
      .send({ from: mainConfig.healthCoinOwnerAddress })
      .on('confirmation', (confirmationNumber: any, receipt: any) => {
        resolve(receipt);
      })
      .on('error', reject);
  });
};

export const createTransaction = async (query: TransactionRequest) => {
  return new Promise((resolve, reject) => {
    getHealthCoinContract().methods.transfer(query.from, query.to, query.amount)
      .send({ from: mainConfig.healthCoinOwnerAddress })
      .on('confirmation', (confirmationNumber: any, receipt: any) => {
        resolve(receipt);
      })
      .on('error', reject);
  });
};

const getHealthCoinContract = () => {
  return new web3.eth.Contract(healthCoinABI.abi, mainConfig.healthCoinAddress);
};
