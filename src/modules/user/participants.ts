import { CreateParticipantRequest } from './typings';

const participants: CreateParticipantRequest[] = [{
    email: 'tjvanderende@hotmail.com',
    password: 'Admin123',
    personProperties: {
        dateOfBirth: '1994-09-06',
        firstName: 'John',
        gender: '1',
        lastName: 'Doe',
        policyNumber: '1233455',
    },
    personalIdNumber: '1234',
}, {
    email: 'tjvanderende@hotmailx.com',
    password: 'Admin123',
    personProperties: {
        dateOfBirth: '1994-09-06',
        firstName: 'Jane',
        gender: '2',
        lastName: 'Doe',
        policyNumber: '12334552332',
    },
    personalIdNumber: '12342333223',
}];

export default participants;
