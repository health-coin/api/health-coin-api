import 'mocha';
import { server } from '../../../src/index';
import * as TreatmentRoute from '../../../src/modules/treatment/treatment.route';
import InvalidPropertyTester from '../utils/InvalidPropertyTester';
import MissingPropertyTester from '../utils/MissingPropertyTester';
import invalidInput from '../validation/invalid/invalid-input';
import validInput from '../validation/valid/valid-input';

const VALID_REQUEST = {
  measurements: validInput.TREATMENT_REQUEST.measurements,
};

const MEASUREMENTS_PROPERTY = 'measurements';

const QUANITY_PROPERTY = 'quantity';
const UNIT_PROPERTY = 'unit';

const invalidTester = new InvalidPropertyTester(VALID_REQUEST, TreatmentRoute.acceptTreatmentplan);
const missingTester = new MissingPropertyTester(VALID_REQUEST, TreatmentRoute.acceptTreatmentplan);

const deleteMeasurementPropertyFunc = (validObject: any, propertyName: string): void => {
  delete validObject[MEASUREMENTS_PROPERTY][0][propertyName];
};

const setMeasurementPropertyFunc = (
  validObject: any, propertyName: string, property: any,
): void => {
  validObject[MEASUREMENTS_PROPERTY][0][propertyName] = property;
};

describe('Add measurement test', () => {
  after('Close server', () => {
    server.close();
  });

  describe(`Without ${MEASUREMENTS_PROPERTY}`, () => {
    missingTester.testProperty(MEASUREMENTS_PROPERTY);
  });

  describe(`With invalid ${MEASUREMENTS_PROPERTY}`, () => {
    invalidTester.testProperty(MEASUREMENTS_PROPERTY, invalidInput.OBECT_IS_STRING);
  });

  describe(`Without ${MEASUREMENTS_PROPERTY} ${QUANITY_PROPERTY}`, () => {
    missingTester.testProperty(QUANITY_PROPERTY, deleteMeasurementPropertyFunc);
  });

  describe(`With invalid ${MEASUREMENTS_PROPERTY} ${QUANITY_PROPERTY}`, () => {
    invalidTester.testProperty(
      QUANITY_PROPERTY, invalidInput.NEGATIVE_AMOUNT, setMeasurementPropertyFunc,
    );
  });

  describe(`Without ${MEASUREMENTS_PROPERTY} ${UNIT_PROPERTY}`, () => {
    missingTester.testProperty(UNIT_PROPERTY, deleteMeasurementPropertyFunc);
  });

  describe(`With invalid ${MEASUREMENTS_PROPERTY} ${UNIT_PROPERTY}`, () => {
    invalidTester.testProperty(
      UNIT_PROPERTY, invalidInput.NAME_CONTAINS_SYMBOL, setMeasurementPropertyFunc,
    );
  });
});
