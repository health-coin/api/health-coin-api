export default {
  ADDRESS_CONTAINS_SYMBOL: '123$aagas',
  DECIMAL_AMOUNT: '1.11',
  ID_TOO_SHORT: '507f1f77bcf86cd79943905',
  METHOD_NONE: 'None',
  NEGATIVE_AMOUNT: '-1',
  NON_EXISTING_ID: '5ca4ec79ded8601c583897fc',
  PATH_CONTAINS_SPACE: '/ me',
  ROLE_CONTAINS_SYMBOL: 'R$le',
  ROLE_NONE: 'None',
  ROUTES_IS_STRING: 'String',

  DATE_CONTAINS_SYMBOL: '2001-03-0$',
  DATE_FUTURE: '2100-03-03',
  DATE_PAST: '2000-03-03',
  EMAIL_NO_AT: 'Abc.gmail.com',
  GENDER_LETTER: 'M',
  NAME_CONTAINS_SYMBOL: 'N#me',
  OBECT_IS_STRING: 'string',
  PASSWORD_CONTAINS_SPACES: 'Pass word',
  PERSONAL_ID_CONTAINS_LETTER: '45892357L',
  POLICY_NUMBER_CONTAINS_LETTER: '45892357L',
  STRING_NONE: 'None',
};
