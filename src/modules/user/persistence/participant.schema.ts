import { Document, model, Model, Schema } from 'mongoose';
import Role from '../Role';
import accountSchema, { Account } from './account.schema';
import { Person, personProperties } from './person.schema';

export const PARTICIPANT_MODEL_NAME = Role.Participant;

const participantProperties = {
  personalIdNumber: {
    required: true,
    type: Number,
  },
};
const participantSchema = new Schema({ ...participantProperties, ...personProperties });

export interface Participant extends Account, Person {
  personalIdNumber: string;
}

type ModelType = Participant & Document;
let participant: Model<ModelType>;
try {
  participant = model<ModelType>(PARTICIPANT_MODEL_NAME);
} catch (err) {
  accountSchema.discriminator(
    PARTICIPANT_MODEL_NAME, participantSchema,
  );
  participant = model<ModelType>(PARTICIPANT_MODEL_NAME, participantSchema);
}
export default participant;
