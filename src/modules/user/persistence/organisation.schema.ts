import { Document, model, Model, Schema } from 'mongoose';
import { Location, LOCATION_MODEL_NAME } from './location.schema';

const { ObjectId } = Schema.Types;
export const ORGANISATION_MODEL_NAME = 'Organisation';

const organisationSchema = new Schema({
  locations: [{
    ref: LOCATION_MODEL_NAME,
    type: ObjectId,
  }],
  name: {
    required: true,
    type: String,
  },
});

export interface Organisation {
  locations: Location[];
  name: string;
}

type ModelType = Location & Document;
let organisation: Model<ModelType>;
try {
  organisation = model<ModelType>(ORGANISATION_MODEL_NAME);
} catch (err) {
  organisation = model<ModelType>(ORGANISATION_MODEL_NAME, organisationSchema);
}
export default organisation;
