import { Error } from '../validation/invalid/error-checks';

export default class PropertyTester {
  protected validObject: any;
  protected routeFunction: (request: any) => Promise<any>;
  protected message: string;
  protected errorCheckFunction: (error: Error) => void;

  constructor(
    validObject: any,
    routeFunction: (request: any) => Promise<any>,
    message: string,
    errorCheckFunction,
  ) {
    this.validObject = validObject;
    this.routeFunction = routeFunction;
    this.message = message;
    this.errorCheckFunction = errorCheckFunction;
  }

  protected checkCloneEquals(clone: any, propertyName: string) {
    if (this.cloneIsEqual(clone)) {
      throw new Error(`Clone is equal to the test object for property: ${propertyName}.`);
    }
  }

  private cloneIsEqual(clone: any): boolean {
    return JSON.stringify(clone) === JSON.stringify(this.validObject);
  }
}
