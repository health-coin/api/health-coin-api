import * as Errors from 'restify-errors';
import * as TransactionService from './transaction.service';
import {
  BalanceQuery, BalanceReponse, RewardTransactionQuery,
  TransactionRequest,
} from './typings';

/**
 * For testing purposes only: Receive total amount of coin supply.
 */
export const getBalance = async (): Promise<BalanceReponse> => {
  const value = await TransactionService.getBalance();
  return { message: 'Totaal balans op contract succesvol opgehaald', data: { balance: value } };
};

export const getUserBalance = async (query: BalanceQuery): Promise<BalanceReponse> => {
  const value = await TransactionService.getUserBalance(query.id);
  return { message: 'Totaal balans van gebruiker succesvol opgehaald', data: { balance: value } };
};

/**
 * Transfer x amount of tokens to a account.
 * (Tokens are coming from contract)
 * TODO: Authentication for "zorgverlener"
 * TODO: Check if "zorgverlener" may reward the "deelnemer".
 * OWNER_ADDRESS should probably the zorgverlener address or somekind.
 */
export const rewardHealthcoins = async (query: RewardTransactionQuery) => {
  await TransactionService.rewardHealthcoins(query);
  const newBalance = await getUserBalance({ id: query.to });

  return {
    data: newBalance,
    message: 'Transactie succesvol',
  };
};

/**
 * This function will transfer an amount of Healhcoins to another user.
 * This is fine for our PoC, but will "to" address will probably be replaced
 * in the future by some kind of "shop"-contract logic.
 * @param query Query object for from address and to address.
 */
export const createTransaction = async (query: TransactionRequest): Promise<BalanceReponse> => {
  const currentBalance = await getUserBalance({ id: query.from });
  if (currentBalance.data.balance < query.amount) {
    throw new Errors.ForbiddenError(`User ${query.from} does not have enough money.`);
  }
  await TransactionService.createTransaction(query);
  const newBalance = await getUserBalance({ id: query.to });
  newBalance.message = 'Transactie succesvol';
  return newBalance;
};
