const gulp = require('gulp');
const ts = require('gulp-typescript');
const del = require('del');
const run = require('gulp-run-command').default;
const tsProject = ts.createProject('tsconfig.json');

const DESTINATION = 'dist';

gulp.task('scripts', () => {
  const tsResult = tsProject.src()
    .pipe(tsProject());
  return tsResult.js.pipe(gulp.dest(DESTINATION));
});

gulp.task('start', run('node dist/src/index.js'))

gulp.task('start-ts', run('ts-node src/index.ts'))
gulp.task('watch', run('nodemon -e ts -w ./src -x gulp start-ts'))
gulp.task('test-local', run('mocha -r ./node_modules/ts-node/register test/tests/**/*.test.ts'))
gulp.task('clean', () => {
  return del([
    'dist/*',
  ]);
});

gulp.task('default', () => {
  const tsResult = tsProject.src()
    .pipe(tsProject());
  return tsResult.js.pipe(gulp.dest(DESTINATION));
});