export interface TokenByIdRequest {
  id: string;
}

export interface CreateTokenRequest {
  address: string;
  role: string;
}

export interface DecodedToken {
  address: string;
  role: string;
}

export interface RevokeTokenRequest {
  accessToken: string;
}

export interface RefreshTokenRequest {
  refreshToken: string;
}

export interface DecodeTokenRequest {
  accessToken: string;
}
