import roleSchema, { Role, Route } from './persistence/role.schema';

export const createRole = async (name: string, allowedRoutes: Route[]): Promise<Role> => {
  return (await roleSchema.create({ name, allowedRoutes })).toObject();
};

export const createOrReplaceRole = async (name: string, allowedRoutes: Route[]): Promise<Role> => {
  const roleFound = await findRole(name);
  if (!roleFound) {
    return createRole(name, allowedRoutes);
  }
  await roleSchema.findByIdAndUpdate(roleFound._id, { name, allowedRoutes });
  return findRole(name);
};

export const findRole = async (name: string): Promise<Role> => {
  return (await roleSchema.findOne({ name }).lean());
};

export const deleteRole = async (name: string) => {
  await roleSchema.findOneAndDelete({ name });
};
