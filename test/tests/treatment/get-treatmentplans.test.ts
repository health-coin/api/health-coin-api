import 'mocha';
import { server } from '../../../src/index';
import * as TreatmentRoute from '../../../src/modules/treatment/treatment.route';
import { GetTreatmentPlansRequest } from '../../../src/modules/treatment/typings';
import InvalidPropertyTester from '../utils/InvalidPropertyTester';
import invalidInput from '../validation/invalid/invalid-input';
import validInput from '../validation/valid/valid-input';

const VALID_REQUEST: GetTreatmentPlansRequest = {
  accessToken: validInput.ACCESS_TOKEN,
};

const FILTER_PROPERTY = 'filter';

const invalidTester = new InvalidPropertyTester(VALID_REQUEST, TreatmentRoute.getTreatmentPlans);

describe('Get treatment plans test', () => {
  after('Close server', () => {
    server.close();
  });

  describe(`With invalid ${FILTER_PROPERTY} invalid object`, () => {
    invalidTester.testProperty(FILTER_PROPERTY, invalidInput.OBECT_IS_STRING);
  });

  describe(`With invalid ${FILTER_PROPERTY} empty object`, () => {
    invalidTester.testProperty(FILTER_PROPERTY, {});
  });

  describe(`With invalid ${FILTER_PROPERTY} non existing state`, () => {
    invalidTester.testProperty(FILTER_PROPERTY, { state: invalidInput.STRING_NONE });
  });

  describe(`With invalid ${FILTER_PROPERTY} non existing state`, () => {
    invalidTester.testProperty(FILTER_PROPERTY, { state: invalidInput.STRING_NONE });
  });
});
