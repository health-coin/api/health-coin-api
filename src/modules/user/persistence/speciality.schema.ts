import { Document, model, Model, Schema } from 'mongoose';

export const SPECIALITY_MODEL_NAME = 'Speciality';

const specialitySchema = new Schema({
  name: {
    required: true,
    type: String,
  },
});

export interface Speciality {
  name: string;
}

type ModelType = Speciality & Document;
let speciality: Model<ModelType>;
try {
  speciality = model<ModelType>(SPECIALITY_MODEL_NAME);
} catch (err) {
  speciality = model<ModelType>(SPECIALITY_MODEL_NAME, specialitySchema);
}
export default speciality;
