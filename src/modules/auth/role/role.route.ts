import { Role } from './persistence/role.schema';
import * as RoleController from './role.controller';
import * as RoleValidator from './role.validator';
import roles from './roles';
import {
  CreateRoleRequest, DeleteRoleRequest, HasRightsRequest, RoleByNameRequest,
} from './typings';

export const createRole = async (request: CreateRoleRequest) => {
  RoleValidator.validateCreateRoleRequest(request);
  return RoleController.createRole(request);
};

export const findRoleByName = async (request: RoleByNameRequest) => {
  RoleValidator.validateFindRoleRequest(request);
  return RoleController.findRoleByName(request);
};

export const deleteRole = async (request: DeleteRoleRequest) => {
  RoleValidator.validateDeleteRoleRequest(request);
  return RoleController.deleteRole(request);
};

export const hasRights = async (request: HasRightsRequest): Promise<void> => {
  RoleValidator.validateHasRightsRequest(request);
  return RoleController.hasRights(request);
};

const loadRoles = async () => {
  const insertRole = async (role: Role) => {
    try {
      await createRole(role);
    } catch (err) {
      console.log(`Not inserting the ${role.name} role it already exists.`);
    }
  };
  await Promise.all(roles.map(role => insertRole(role)));
};

(async () => await loadRoles())();
