import { BAD_REQUEST_MESSAGE, createClone } from '../test-utils';
import * as ErrorChecks from '../validation/invalid/error-checks';
import PropertyTester from './PropertyTester';

export default class MissingPropertyTester extends PropertyTester {

  public constructor(
    validObject: any,
    routeFunction: (request: any) => Promise<any>,
    message: string = BAD_REQUEST_MESSAGE,
    errorCheckFunction = ErrorChecks.validateBadRequest,
    ) {
    super(validObject, routeFunction, message, errorCheckFunction);
  }

  public testProperty(
      propertyName: string,
      deletePropertyFunction: (
          validObject: any, propertyName: string,
      ) => void = deletePropertyFunc,
    ) {
    it(this.message, async () => {
      const clone = createClone(this.validObject);
      deletePropertyFunction(clone, propertyName);
      this.checkCloneEquals(clone, propertyName);

      let exists = false;
      try {
        await this.routeFunction(clone);
        exists = true;
      } catch (err) {
        this.errorCheckFunction(err.body);
      }
      if (exists) {
        throwExistsError(this.validObject, propertyName);
      }
    });
  }

}

const deletePropertyFunc = (validObject: any, propertyName: string): void => {
  delete validObject[propertyName];
};

const throwExistsError = (validObject: any, propertyName: string) => {
  throw new Error(`Route returns a result for object: ${JSON.stringify(validObject)} and
    property ${propertyName}`,
  );
};
