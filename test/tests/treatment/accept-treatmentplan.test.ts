import 'mocha';
import { server } from '../../../src/index';
import * as TreatmentRoute from '../../../src/modules/treatment/treatment.route';
import InvalidPropertyTester from '../utils/InvalidPropertyTester';
import MissingPropertyTester from '../utils/MissingPropertyTester';
import invalidInput from '../validation/invalid/invalid-input';
import validInput from '../validation/valid/valid-input';

const VALID_REQUEST = {
  id: validInput.ID,
};

const ID_PROPERTY = 'id';

const invalidTester = new InvalidPropertyTester(VALID_REQUEST, TreatmentRoute.acceptTreatmentplan);
const missingTester = new MissingPropertyTester(VALID_REQUEST, TreatmentRoute.acceptTreatmentplan);

describe('Accept treatment plan test', () => {
  after('Close server', () => {
    server.close();
  });

  describe(`Without ${ID_PROPERTY}`, () => {
    missingTester.testProperty(ID_PROPERTY);
  });

  describe(`With invalid ${ID_PROPERTY}`, () => {
    invalidTester.testProperty(ID_PROPERTY, invalidInput.ID_TOO_SHORT);
  });
});
