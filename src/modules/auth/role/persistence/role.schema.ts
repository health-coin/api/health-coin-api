import { Document, model, Model, Schema } from 'mongoose';

export const ROLE_MODEL_NAME = 'Role';

const roleSchema = new Schema({
  allowedRoutes: [{
    method: String,
    path: String,
  }],
  name: {
    required: true,
    type: String,
  },
});

export interface Route {
  path: string;
  method: string;
}

export interface Role {
  _id?: string;
  name: string;
  allowedRoutes: Route[];
}

type ModelType = Role & Document;
let role: Model<ModelType>;
try {
  role = model<ModelType>(ROLE_MODEL_NAME);
} catch (err) {
  role = model<ModelType>(ROLE_MODEL_NAME, roleSchema);
}
export default role;
