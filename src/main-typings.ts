interface Route {
  path: string;
  method: string;
}

export interface SuccessResponse {
  message: string;
  data: any;
}

export interface HasRightsRequest extends AccessTokenRequest {
  route: Route;
}

interface PersonProperties {
  dateOfBirth: string;
  firstName: string;
  gender: string;
  lastName: string;
  policyNumber: string;
}

export interface GetUserBalanceRequest {
  accessToken: string;
}

export interface AccessTokenRequest {
  accessToken: string;
}

export interface GetCareProviderRequest {
  accessToken: string;
  careProviderAddress: string;
}
export interface GetParticipantRequest {
  accessToken: string;
  participantAddress: string;
}
export interface CreateTransactionRequest extends AccessTokenRequest {
  to: string;
  amount: number;
}

export interface CreateParticipantRequest {
  address?: string;
  email: string;
  password: string;
  personProperties: PersonProperties;
  personalIdNumber: string;
}

export interface LoginRequest {
  email: string;
  password: string;
}

export interface ValueProperties {
  quantity: string;
  unit: string;
}

export interface StatusProperties {
  status: string;
}
