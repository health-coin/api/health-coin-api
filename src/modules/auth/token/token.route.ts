import { Token } from './persistence/token.schema';
import * as TokenController from './token.controller';
import * as TokenValidator from './token.validator';
import {
  CreateTokenRequest, DecodedToken, DecodeTokenRequest,
  RefreshTokenRequest, RevokeTokenRequest, TokenByIdRequest,
} from './typings';

export const findTokenById = async (request: TokenByIdRequest): Promise<Token> => {
  TokenValidator.validateFindTokenRequest(request);
  return TokenController.findTokenById(request);
};

export const createToken = async (request: CreateTokenRequest): Promise<Token> => {
  TokenValidator.validateCreateTokenRequest(request);
  return TokenController.createToken(request);
};

export const revokeToken = async (request: RevokeTokenRequest) => {
  TokenValidator.validateRevokeTokenRequest(request);
  return TokenController.revokeToken(request);
};

export const refreshToken = async (request: RefreshTokenRequest): Promise<Token> => {
  TokenValidator.validateRefreshTokenRequest(request);
  return TokenController.refreshToken(request);
};

export const decodeToken = async (request: DecodeTokenRequest): Promise<DecodedToken> => {
  TokenValidator.validateDecodeRequest(request);
  const decoded = await TokenController.decodeToken(request);
  return { address: decoded.address, role: decoded.role };
};
