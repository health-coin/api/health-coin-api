import * as Bcrypt from 'bcrypt';
import * as Errors from 'restify-errors';
import { web3 } from '../..';
import { Account } from './persistence/account.schema';
import { Participant } from './persistence/participant.schema';
import {
  CreateParticipantRequest, GetCareProviderRequest, GetParticipantByIdRequest, GetParticipantRequest, LoginRequest, SuccessResponse,
} from './typings';
import * as UserService from './user.service';

const MAX_ACCOUNT_UNLOCK_SECONDS = 9223372036;
const SALT_ROUNDS = 10;

const deletePassword = (account: Account) => {
  delete account.password;
};

export const createParticipant = async (
  request: CreateParticipantRequest,
): Promise<SuccessResponse> => {

  const { personalIdNumber, personProperties, email, password } = request;
  const { policyNumber } = personProperties;

  checkParticipantExists(
    await UserService.findParticipant({ personalIdNumber }),
    `personalIdNummer ${personalIdNumber} is al in geruik`,
  );
  checkParticipantExists(
    await UserService.findParticipant({ policyNumber }),
    `policydNummer ${policyNumber} is al in geruik`,
  );
  checkParticipantExists(
    await UserService.findParticipant({ email }),
    'Emailadres is al in gebruik',
  );
  const address = await createEthereumAccount(password);
  const hashedPassword = await hashPassword(password);
  request.password = hashedPassword;
  request.address = address;
  delete request.personProperties;

  const participant = await UserService.createParticipant({ ...request, ...personProperties });
  deletePassword(participant);
  return {
    data: participant,
    message: 'Gebruiker succesvol toegevoegd.',
  };
};

export const hashPassword = async (password: string) => {
  return Bcrypt.hash(password, SALT_ROUNDS);
};

const checkParticipantExists = (participant: Participant, errorMessage: string) => {
  if (participant) {
    throw new Errors.BadRequestError(errorMessage);
  }
};

export const createEthereumAccount = async (password: string): Promise<string> => {
  const account = await web3.eth.personal.newAccount(password);
  await web3.eth.personal.unlockAccount(account, password, MAX_ACCOUNT_UNLOCK_SECONDS);
  return account;
};

export const loginUser = async (request: LoginRequest) => {
  const { email, password } = request;
  const invalidMessage = `${password} of ${email} zijn ongeldig`;

  try {
    const userFound = await UserService.findUserByEmail(email);
    if (!userFound) {
      throw new Errors.UnauthorizedError(invalidMessage);
    }
    if (!(await Bcrypt.compare(password, userFound.password))) {
      throw new Errors.UnauthorizedError(invalidMessage);
    }

    deletePassword(userFound);
    return {
      data: userFound,
      message: 'Gebruiker succesvol ingelogd.',
    };
  } catch (err) {
    throw new Errors.UnauthorizedError(invalidMessage);
  }
};

export const getParticipants = async () => {
  const participants = await UserService.findParticipants();
  return {
    data: participants,
    message: 'Deelnemers succesvol opgehaald',
  };
};

export const getCareProvider = async (request: GetCareProviderRequest) => {
  const { userAddress } = request;
  const careProviderFound = await UserService.findCareProviderByAddress(userAddress);
  if (!careProviderFound) {
    throw new Errors.NotFoundError(`Zorgprofessional met address ${userAddress} bestaat niet.`);
  }
  deletePassword(careProviderFound);
  return {
    data: careProviderFound,
    message: 'Zorgprofessional succesvol opgehaald',
  };
};

export const getParticipant = async (request: GetParticipantRequest) => {
  const { userAddress } = request;
  const participantFound = await UserService.findParticipantByAddress(userAddress);
  if (!participantFound) {
    throw new Errors.NotFoundError(`Deelnemer met address ${userAddress} bestaat niet.`);
  }
  deletePassword(participantFound);
  return {
    data: participantFound,
    message: 'Deelnemer succesvol opgehaald',
  };
};

export const getParticipantById = async (request: GetParticipantByIdRequest) => {
  const { userId } = request;
  const participantFound = await UserService.findParticipantById(userId);
  if (!participantFound) {
    throw new Errors.NotFoundError(`Deelnemer met id ${userId} bestaat niet.`);
  }
  deletePassword(participantFound);
  return {
    data: participantFound,
    message: 'Deelnemer succesvol opgehaald',
  };
};
