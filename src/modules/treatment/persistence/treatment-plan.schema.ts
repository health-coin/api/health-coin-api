import { Document, model, Model, Schema } from 'mongoose';
import { CAREPROVIDER_MODEL_NAME } from '../../user/persistence/care-provider.schema';
import { PARTICIPANT_MODEL_NAME } from '../../user/persistence/participant.schema';
import { MEASUREMENT_MODEL_NAME } from './measurement.schema';

export const TREATMENT_PLAN_MODEL_NAME = 'Treatmentplan';
const { ObjectId } = Schema.Types;

const planSchema = new Schema({
  careProviderId: {
    ref: CAREPROVIDER_MODEL_NAME,
    required: true,
    type: ObjectId,
  },
  contractAddress: {
    required: false,
    type: String,
  },
  customMeasurements: [{
    ref: MEASUREMENT_MODEL_NAME,
    required: false,
    type: ObjectId,
  }],
  participantId: {
    ref: PARTICIPANT_MODEL_NAME,
    required: true,
    type: ObjectId,
  },
});

export interface TreatmentPlan {
  _id?: string;
  contractAddress: string;
  endDate: Date;
  startDate?: Date;
  participantId: string;
  careProviderId: string;
  customMeasurements: any[];
}

type ModelType = TreatmentPlan & Document;
let treatmentPlan: Model<ModelType>;
try {
  treatmentPlan = model<ModelType>(TREATMENT_PLAN_MODEL_NAME);
} catch (err) {
  treatmentPlan = model<ModelType>(TREATMENT_PLAN_MODEL_NAME, planSchema);
}
export default treatmentPlan;
