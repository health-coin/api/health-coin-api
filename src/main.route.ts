import { Next, Request, Response } from 'restify';
import * as Errors from 'restify-errors';
import * as MainController from './main.controller';

const validateAndFormatAccessToken = (req: Request) => {
  if (!req.headers || !req.headers.authorization) {
    throw new Errors.BadRequestError('Geen accessToken headers gevonden.');
  }
  if (!req.headers.authorization.match('Bearer ')) {
    throw new Errors.BadRequestError('Authorization token begint niet met \'Bearer\'');
  }
  req.headers.authorization = req.headers.authorization.replace('Bearer ', '');
};

export const getUserBalance = async (req: Request, res: Response, next: Next) => {
  try {
    res.send(await MainController.getUserBalance({ accessToken: req.headers.authorization }));
    return next();
  } catch (err) {
    return next(err);
  }
};

export const createTransaction = async (req: Request, res: Response, next: Next) => {
  try {
    const tokenRequest = { accessToken: req.headers.authorization };
    res.send(await MainController.createTransaction({ ...tokenRequest, ...req.body }));
    return next();
  } catch (err) {
    return next(err);
  }
};

export const createParticipant = async (req: Request, res: Response, next: Next) => {
  try {
    res.send(await MainController.createParticipant(req.body));
    return next();
  } catch (err) {
    return next(err);
  }
};

export const getParticipant = async (req: Request, res: Response, next: Next) => {
  try {
    const request = {
      accessToken: req.headers.authorization,
      participantAddress: req.params.address,
    };
    res.send(await MainController.getParticipant(request));
    return next();
  } catch (err) {
    return next(err);
  }
};

export const getParticipants = async (req: Request, res: Response, next: Next) => {
  try {
    const request = {
      accessToken: req.headers.authorization,
    };
    res.send(await MainController.getParticipants(request));
    return next();
  } catch (err) {
    return next(err);
  }
};

export const getAllParticipants = async (req: Request, res: Response, next: Next) => {
  try {
    res.send(await MainController.getAllParticipants());
    return next();
  } catch (err) {
    return next(err);
  }
};

export const getCareProvider = async (req: Request, res: Response, next: Next) => {
  try {
    const request = {
      accessToken: req.headers.authorization,
      careProviderAddress: req.params.address,
    };
    res.send(await MainController.getCareProvider(request));
    return next();
  } catch (err) {
    return next(err);
  }
};

export const hasRights = async (req: Request, res: Response, next: Next) => {
  try {
    validateAndFormatAccessToken(req);
    const { authorization } = req.headers;
    const { path, method } = req.getRoute();
    const route = { method, path: path.toString() };
    await MainController.hasRights({ accessToken: authorization, route });
    return next();
  } catch (err) {
    return next(err);
  }
};

export const loginUser = async (req: Request, res: Response, next: Next) => {
  try {
    res.send(await MainController.loginUser(req.body));
    return next();
  } catch (err) {
    return next(err);
  }
};

export const createTreatmentplan = async (req: Request, res: Response, next: Next) => {
  try {
    res.send(await MainController.createTreatmentplan(
      {
        ...req.body,
        accessToken: req.headers.authorization,
      }));
    return next();
  } catch (err) {
    return next(err);
  }
};

export const findTreatmentPlans = async (req: Request, res: Response, next: Next) => {
  try {
    res.send(await MainController.findTreatmentPlans(
      {
        ...req.body,
        accessToken: req.headers.authorization,
        filter: req.query,
      },
    ));
    return next();
  } catch (err) {
    return next(err);
  }
};

export const findTreatmentplanById = async (req: Request, res: Response, next: Next) => {
  try {
    res.send(await MainController.findTreatmentplanById(
      {
        accessToken: req.headers.authorization,
        ...req.params,
      }));
    return next();
  } catch (err) {
    return next(err);
  }
};

export const findActiveTreatmentPlan = async (req: Request, res: Response, next: Next) => {
  try {
    res.send(await MainController.findActiveTreatmentplan(
      {
        accessToken: req.headers.authorization,
        ...req.params,
      }));
    return next();
  } catch (err) {
    return next(err);
  }
};

export const acceptTreatmentplan = async (req: Request, res: Response, next: Next) => {
  try {
    res.send(await MainController.acceptTreatmentPlan(
      {
        accessToken: req.headers.authorization,
        ...req.params,
      }));
    return next();
  } catch (err) {
    return next(err);
  }
};

export const addMeasurement = async (req: Request, res: Response, next: Next) => {
  try {
    res.send(await MainController.addMeasurement(
      {
        measurements: req.body,
        treatmentData: {
          accessToken: req.headers.authorization,
          id: req.params.id,
        },
      },
    ));
    return next();
  } catch (err) {
    return next(err);
  }
};
