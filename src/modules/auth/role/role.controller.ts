import * as Errors from 'restify-errors';
import { runInContext } from 'vm';
import * as TokenRoute from '../token/token.route';
import { Role } from './persistence/role.schema';
import * as RoleService from './role.service';
import {
   CreateRoleRequest, DeleteRoleRequest, HasRightsRequest, RoleByNameRequest,
} from './typings';

export const createRole = async (request: CreateRoleRequest): Promise<Role> => {
  const { name, allowedRoutes } = request;
  const roleFound = await RoleService.findRole(name);
  if (roleFound) {
    throw new Errors.ConflictError(`Role met naam '${name}' bestaat al.`);
  }
  return RoleService.createRole(name, allowedRoutes);
};

export const createOrReplaceRole = async (request: CreateRoleRequest): Promise<Role> => {
  return RoleService.createOrReplaceRole(request.name, request.allowedRoutes);
};

export const findRoleByName = async (request: RoleByNameRequest): Promise<Role> => {
  const { name } = request;
  const roleFound = await RoleService.findRole(name);
  if (!roleFound) {
    throw new Errors.NotFoundError(`Role met naam '${name}' bestaat niet.`);
  }
  return roleFound;
};

export const deleteRole = async (request: DeleteRoleRequest) => {
  const { name } = request;
  return RoleService.deleteRole(name);
};

export const hasRights = async (request: HasRightsRequest) => {
  const { role, route } = request;
  const roleFound = await RoleService.findRole(role);
  const noPermMessage = `Role ${role} heeft geen permissie voor ${route.method} ${route.path}`;
  if (!roleFound) {
    throw new Errors.ForbiddenError(noPermMessage);
  }
  const routesFound = roleFound.allowedRoutes.filter(
    _route => _route.method === route.method && _route.path === route.path,
  );
  if (routesFound.length <= 0) {
    throw new Errors.ForbiddenError(noPermMessage);
  }
};
