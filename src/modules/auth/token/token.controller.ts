import * as Jwt from 'jsonwebtoken';
import * as Errors from 'restify-errors';
import mainConfig from '../../../../config/main.config';
import * as RoleRoute from '../role/role.route';
import { Token } from './persistence/token.schema';
import * as TokenService from './token.service';
import {
   CreateTokenRequest, DecodedToken, DecodeTokenRequest,
   RefreshTokenRequest, RevokeTokenRequest, TokenByIdRequest,
} from './typings';

export const findTokenById = async (request: TokenByIdRequest): Promise<Token> => {
  const { id } = request;
  const token = await TokenService.findTokenById(id);
  if (!token) {
    throw new Errors.NotFoundError(`Token met id '${id}' bestaat niet`);
  }
  return token;
};

export const createToken = async (request: CreateTokenRequest): Promise<Token> => {
  const NOT_FOUND_MESSAGE = `${request.role} role bestaat niet`;
  try {
    const roleFound = await RoleRoute.findRoleByName({ name: request.role });
    if (!roleFound) {
      throw new Errors.BadRequestError(NOT_FOUND_MESSAGE);
    }
  } catch (err) {
    throw new Errors.BadRequestError(NOT_FOUND_MESSAGE);
  }

  const expirationDate = new Date();
  expirationDate.setMinutes(expirationDate.getMinutes() + mainConfig.validTokenMinutes);
  const { address, role } = request;

  const accessToken = Jwt.sign(
    { address, role, exp: expirationDate.getTime() }, mainConfig.accessTokenKey,
  );
  const refToken = Jwt.sign(
    { address, role, created: new Date() }, mainConfig.refreshTokenKey,
  );
  const token = await TokenService.createToken(accessToken, refToken);
  token.expirationDate = expirationDate;
  return token;
};

export const revokeToken = async (request: RevokeTokenRequest) => {
  const { accessToken } = request;
  const tokenFound = await TokenService.findTokenByAccessToken(accessToken);
  if (!tokenFound) {
    throw new Errors.NotFoundError(`Token met accessToken '${accessToken}' bestaat niet`);
  }
  await TokenService.deleteTokenById(tokenFound._id);
};

export const refreshToken = async (request: RefreshTokenRequest) => {
  const INVALID_MESSAGE = 'refreshToken is ongeldig.';
  const refToken = request.refreshToken;

  const tokenFound = await TokenService.findTokenByRefreshToken(refToken);
  if (!tokenFound) {
    throw new Errors.UnauthorizedError(INVALID_MESSAGE);
  }
  try {
    const decoded = await Jwt.verify(refToken, mainConfig.refreshTokenKey);
    await TokenService.deleteTokenById(tokenFound._id);
    return createToken(decoded);
  } catch (err) {
    throw new Errors.UnauthorizedError(INVALID_MESSAGE);
  }
};

export const decodeToken = async (request: DecodeTokenRequest): Promise<DecodedToken> => {
  const { accessToken } = request;
  const INVALID_MESSAGE = 'accessToken is ongeldig.';

  const tokenFound = await TokenService.findTokenByAccessToken(accessToken);
  if (!tokenFound) {
    throw new Errors.UnauthorizedError(INVALID_MESSAGE);
  }
  try {
    const decoded = await Jwt.verify(accessToken, mainConfig.accessTokenKey);
    return decoded;
  } catch (err) {
    throw new Errors.UnauthorizedError(INVALID_MESSAGE);
  }
};
