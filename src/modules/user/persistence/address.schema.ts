import { Document, model, Model, Schema } from 'mongoose';

export const ADDRESS_MODEL_NAME = 'Address';
const COUNTY_NETHERLANDS = 'Netherlands';

const addressSchema = new Schema({
  addition: {
    required: false,
    type: String,
  },
  city: {
    required: true,
    type: String,
  },
  country: {
    default: COUNTY_NETHERLANDS,
    required: true,
    type: String,
  },
  postalCode: {
    required: true,
    type: String,
  },
  street: {
    required: true,
    type: String,
  },
  streetNumber: {
    required: true,
    type: Number,
  },
});

export interface Address {
  addition: string;
  city: string;
  country: string;
  postalCode: string;
  street: string;
  streetNumber: number;
}

type ModelType = Address & Document;
let address: Model<ModelType>;
try {
  address = model<ModelType>(ADDRESS_MODEL_NAME);
} catch (err) {
  address = model<ModelType>(ADDRESS_MODEL_NAME, addressSchema);
}
export default address;
