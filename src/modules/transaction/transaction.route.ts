import * as TransactionController from './transaction.controller';
import * as TransactionValidator from './transaction.validator';
import { BalanceQuery, RewardTransactionQuery, TransactionRequest } from './typings';

export const getBalance = async () => {
  return TransactionController.getBalance();
};

export const getUserBalance = async (query: BalanceQuery) => {
  await TransactionValidator.validateBalanceRequest(query);
  return TransactionController.getUserBalance(query);
};

export const rewardHealthcoins = async (query: RewardTransactionQuery) => {
  return TransactionController.rewardHealthcoins(query);
};

export const createTransaction = async (request: TransactionRequest) => {
  await TransactionValidator.validateTransferRequest(request);
  return TransactionController.createTransaction(request);
};
