/*const ProviderEngine = require("web3-provider-engine");
const RpcProvider = require("web3-provider-engine/subproviders/rpc.js");
const { TruffleArtifactAdapter } = require("@0x/sol-trace");
const { GanacheSubprovider } = require("@0x/subproviders");
const { ProfilerSubprovider } = require("@0x/sol-profiler");
const { CoverageSubprovider } = require("@0x/sol-coverage");
const { RevertTraceSubprovider } = require("@0x/sol-trace");
const projectRoot = ".";
const solcVersion = "0.5.0";
const defaultFromAddress = "0x5409ed021d9299bf6814279a6a1411a7e866a631";
const artifactAdapter = new TruffleArtifactAdapter(projectRoot, solcVersion);
const revertTraceSubprovider = new RevertTraceSubprovider(artifactAdapter, defaultFromAddress);

const providerEngine = new ProviderEngine();
providerEngine.addProvider(revertTraceSubprovider);
providerEngine.addProvider(new RpcSubprovider({ rpcUrl: 'http://localhost:7545' }));
providerEngine.start();
*/
module.exports = {
    networks: {
        //...
        proxy: {
            host: "127.0.0.1",
            port: 9545,
            network_id: "*",
            gasPrice: 0
        },
        ganache: {
            host: "127.0.0.1",
            port: 7545,
            network_id: 5777,
            gasPrice: 0
        },
        //...
    }
};

