import { ObjectId } from 'mongodb';
import measurementSchema, { Measurement } from './persistence/measurement.schema';
import treatmentPlanSchema, { TreatmentPlan } from './persistence/treatment-plan.schema';

export const createTreatmentplan = async (request: any): Promise<TreatmentPlan> => {
  return (await treatmentPlanSchema.create(request)).toObject();
};

export const getTreatmentplans = async (): Promise<TreatmentPlan[]> => {
  try {
    return treatmentPlanSchema.find();
  } catch (erx) {
    throw new Error('Geen behandelplannen gevonden');
  }
};

export const getTreamentplansByParticipantOrCareProvider = async (id: string):
  Promise<TreatmentPlan[]> => {
  try {
    return treatmentPlanSchema.find()
      .or([{ participantId: id }, { careProviderId: id }])
      .populate('customMeasurements');
  } catch (erx) {
    throw new Error('Geen behandelplannen gevonden');
  }
};

export const getTreatmentplanById = async (id: string): Promise<TreatmentPlan> => {
  return treatmentPlanSchema.findOne({ _id: new ObjectId(id) })
    .populate('customMeasurements')
    .lean();
};

export const updateTreatmentContractAddress = async (_id: string, contractAddress):
  Promise<void> => {
  return treatmentPlanSchema.updateOne({ _id }, { $set: { contractAddress } });
};

export const updateTreatmentMeasurements = async (_id: string, measurements):
  Promise<void> => {
  return treatmentPlanSchema.updateOne({ _id }, { $set: { customMeasurements: measurements } });
};

export const addCustomMeasurementToTreatment = async (
  treatmentId: string, request: Measurement,
) => {
  return treatmentPlanSchema.findOne({ _id: treatmentId }).exec(
    async (error, treatment) => {
      const measurement = await measurementSchema.create(request);
      const measurements = treatment.customMeasurements !== null ?
        treatment.customMeasurements : [];
      measurements.push(measurement._id);
      await treatment.save();
    });
};
