import * as Errors from 'restify-errors';
import * as ValidatorFuncs from '../validator-funcs';
import { Route } from './persistence/role.schema';
import {
   CreateRoleRequest, DeleteRoleRequest, HasRightsRequest, RoleByNameRequest,
} from './typings';

const validateRoute = (route: Route) => {
  const PATH_PROPERTY = 'path';
  const METHOD_PROPERTY = 'method';
  const path = route[PATH_PROPERTY];
  const method = route[METHOD_PROPERTY];

  checkRequired(path, PATH_PROPERTY);
  checkRequired(method, METHOD_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidPath(x), path, PATH_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidMethod(x), method, METHOD_PROPERTY);
};

export const validateCreateRoleRequest = (request: CreateRoleRequest) => {
  const NAME_PROPERTY = 'name';
  const ROUTES_PROPERTY = 'allowedRoutes';
  const { name, allowedRoutes } = request;

  checkRequired(name, NAME_PROPERTY);
  checkRequired(allowedRoutes, ROUTES_PROPERTY);
  checkValid(x => !Array.isArray(x), allowedRoutes, ROUTES_PROPERTY);
  allowedRoutes.forEach(validateRoute);
  checkValid(x => ValidatorFuncs.invalidRole(x), name, NAME_PROPERTY);
};

export const validateFindRoleRequest = (request: RoleByNameRequest) => {
  validateRoleName(request);
};

export const validateDeleteRoleRequest = (request: DeleteRoleRequest) => {
  validateRoleName(request);
};

export const validateHasRightsRequest = (request: HasRightsRequest) => {
  const ROLE_PROPERTY = 'role';
  const ROUTE_PROPERTY = 'route';
  const PATH_PROPERTY = 'path';
  const METHOD_PROPERTY = 'method';
  const role = request[ROLE_PROPERTY];
  const route = request[ROUTE_PROPERTY];
  checkRequired(route, ROUTE_PROPERTY);

  const path = request[ROUTE_PROPERTY][PATH_PROPERTY];
  const method = request[ROUTE_PROPERTY][METHOD_PROPERTY];

  checkRequired(role, ROLE_PROPERTY);
  checkRequired(path, PATH_PROPERTY);
  checkRequired(method, METHOD_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidRole(x), role, ROLE_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidPath(x), path, PATH_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidMethod(x), method, METHOD_PROPERTY);
};

const validateRoleName = (request: any) => {
  const NAME_PROPERTY = 'name';
  const name = request[NAME_PROPERTY];

  checkRequired(name, NAME_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidRole(x), name, NAME_PROPERTY);
};

const checkRequired = (property: any, propertyName: string) => {
  if (!property) {
    throw new Errors.BadRequestError(`${propertyName} is verplicht`);
  }
};

const checkValid = (
  isInvalidFunction: (property: any) => boolean, property: any, propertyName: string,
) => {
  if (isInvalidFunction(property)) {
    throw new Errors.BadRequestError(`${propertyName} is ongeldig`);
  }
};
