import * as Chai from 'chai';
import 'mocha';
import { server } from '../../../src/index';
import { HasRightsRequest } from '../../../src/main-typings';
import * as MainController from '../../../src/main.controller';
import { Role } from '../../../src/modules/auth/role/persistence/role.schema';
import { Token } from '../../../src/modules/auth/token/persistence/token.schema';
import * as PersistenceUtils from '../persistence-utils';
import * as TestUtils from '../test-utils';
import InvalidPropertyTester from '../utils/InvalidPropertyTester';
import * as ErrorChecks from '../validation/invalid/error-checks';
import validInput from '../validation/valid/valid-input';

const should = Chai.should();
const VALID_REQUEST: HasRightsRequest = {
  accessToken : validInput.ACCESS_TOKEN,
  route: {
    method: validInput.METHOD_GET,
    path: validInput.PATH_ME,
  },
};

const ACCESS_TOKEN_PROPERTY = 'accessToken';
const ROUTE_PROPERTY = 'route';
const PATH_PROPERTY = 'path';
const METHOD_PROPERTY = 'method';

const unauthorizedTester = new InvalidPropertyTester(
  VALID_REQUEST,
  MainController.hasRights,
  TestUtils.UNAUTHORIZED_MESSAGE,
  ErrorChecks.validateUnAuthorized,
);

const forbiddenTester = new InvalidPropertyTester(
  VALID_REQUEST,
  MainController.hasRights,
  TestUtils.FORBIDDEN_MESSAGE,
  ErrorChecks.validateForbidden,
);

const setRoutePropertyFunc = (validObject: any, propertyName: string, property: any): void => {
  validObject[ROUTE_PROPERTY][propertyName] = property;
};

describe('Has rights integration tests', () => {
  let createdRole: Role;
  let forbiddenRole: Role;
  let validToken: Token;
  let forbiddenToken: Token;

  before('Create test objects', async () => {
    createdRole = await PersistenceUtils.getOrCreateRole();
    forbiddenRole = await PersistenceUtils.getOrCreateRole(validInput.ROLE_ADMIN, [{
      method: validInput.METHOD_DELETE,
      path: validInput.PATH_ME,
    }]);
    validToken = await PersistenceUtils.createToken(validInput.ADDRESS, createdRole.name);
    forbiddenToken = await PersistenceUtils.createToken(validInput.ADDRESS, forbiddenRole.name);
    VALID_REQUEST[ACCESS_TOKEN_PROPERTY] = validToken.accessToken;
  });

  after('Delete test objects', async () => {
    await PersistenceUtils.deleteRole(createdRole.name);
    await PersistenceUtils.deleteRole(forbiddenRole.name);
    await PersistenceUtils.deleteToken(validToken.accessToken);
    await PersistenceUtils.deleteToken(forbiddenToken.accessToken);
    server.close();
  });

  describe(`With an invalid ${ACCESS_TOKEN_PROPERTY}`, () => {
    unauthorizedTester.testProperty(ACCESS_TOKEN_PROPERTY, validInput.ACCESS_TOKEN);
  });

  describe(`With an invalid ${PATH_PROPERTY}`, () => {
    forbiddenTester.testProperty(PATH_PROPERTY, validInput.PATH_BALANCE, setRoutePropertyFunc);
  });

  describe(`With an invalid ${METHOD_PROPERTY}`, () => {
    forbiddenTester.testProperty(METHOD_PROPERTY, validInput.METHOD_DELETE, setRoutePropertyFunc);
  });

  describe('With a different role without access', () => {
    it(TestUtils.UNAUTHORIZED_MESSAGE, () => {
      forbiddenTester.testProperty(ACCESS_TOKEN_PROPERTY, forbiddenToken.accessToken);
    });
  });

  describe('With the valid token', () => {
    it('Should return void', async () => {
      const result = await MainController.hasRights(VALID_REQUEST);
      should.not.exist(result);
    });
  });
});
