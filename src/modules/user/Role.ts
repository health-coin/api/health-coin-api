enum Role {
    Participant = 'Participant',
    CareProvider = 'CareProvider',
}

export default Role;
