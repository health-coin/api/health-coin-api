const mongoSanitize = require('mongo-sanitize');

export const cleanRequest = (req: any) => {
  if (!req) {
    return req;
  }
  Object.keys(req).forEach((key) => {

    if (typeof req[key] === 'object') {
      cleanRequest(req[key]);
    } else {
      const value = req[key];
      if (value === undefined || value === '') {
        delete req[key];
      } else {
        req[key] = mongoSanitize(value);
      }
    }
  });
};
