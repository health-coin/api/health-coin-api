const HealthCoin = artifacts.require('./Token/HealthCoin.sol');
const Owner = artifacts.require('./Token/Owner.sol');
module.exports = (deployer) => {
    deployer.deploy(HealthCoin);
    deployer.deploy(Owner);
}