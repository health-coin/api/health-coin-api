import * as Moment from 'moment';
import * as Validator from 'validator';
import { web3 } from '../../index';

const ID_REGEX = /^[a-fA-F0-9]{24}$/;
const PASSWORD_REGEX = /^((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,50})$/;
const DAY_DATE_REGEX = /^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])$/;
const NAME_REGEX = /^[a-zA-Z ,.'-]+$/;
const NUMBER_REGEX = /^\d+$/;
const GENDER_VALUES = ['1', '2', '3'];

export const invalidAddress = (address: string) => {
  return !web3.utils.isAddress(address);
};

export const invalidId = (id: string) => {
  return !ID_REGEX.test(id);
};

export const invalidEmail = (email: string) => {
  return !Validator.isEmail(email);
};

export const invalidPassword = (password: string) => {
  return !PASSWORD_REGEX.test(password);
};

const invalidateDateDay = (dateString: string) => {
  if (!DAY_DATE_REGEX.test(dateString)) {
    return true;
  }
  const date = Moment(dateString);
  return !date.isValid();
};

export const invalidDateOfBirth = (dateString: string) => {
  if (invalidateDateDay(dateString)) {
    return true;
  }
  const date = new Date(dateString);
  return date >= new Date();
};

export const invalidName = (name: string) => {
  return !NAME_REGEX.test(name);
};

export const invalidGender = (gender: string) => {
  return !GENDER_VALUES.includes(gender.toString());
};

export const invalidPolicyNumber = (num: any) => {
  return !NUMBER_REGEX.test(num);
};

export const invalidPersonalNumber = (num: any) => {
  return !NUMBER_REGEX.test(num);
};

export const invalidObject = (object: any) => {
  return !(object instanceof Object);
};
