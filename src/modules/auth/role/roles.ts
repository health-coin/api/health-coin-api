import { Role } from './persistence/role.schema';

const METHODS = {
  DELETE: 'DELETE',
  GET: 'GET',
  PATCH: 'PATCH',
  POST: 'POST',
  PUT: 'PUT',
};

const roles: Role[] = [{
  allowedRoutes: [
    {
      method: METHODS.GET,
      path: '/user/balance',
    },
    {
      method: METHODS.POST,
      path: '/transactions',
    },
    {
      method: METHODS.POST,
      path: '/treatmentplans/:id/sign',
    },
    {
      method: METHODS.GET,
      path: '/treatmentplans/:id',
    },
    {
      method: METHODS.GET,
      path: '/treatmentplans',
    },
    {
      method: METHODS.GET,
      path: '/treatmentplans/active',
    },
    {
      method: METHODS.POST,
      path: '/treatmentplans/:id/measurements',
    },
    {
      method: METHODS.GET,
      path: '/participant/careprofessionals/:address',
    },
  ],
  name: 'Participant',
}, {
  allowedRoutes: [
    {
      method: METHODS.GET,
      path: '/treatmentplans',
    },
    {
      method: METHODS.POST,
      path: '/treatmentplans',
    },
    {
      method: METHODS.GET,
      path: '/treatmentplans/:id',
    },
    {
      method: METHODS.POST,
      path: '/treatmentplans/:id/measurements',
    },
    {
      method: METHODS.GET,
      path: '/careprofessional/participants/:address',
    },
    {
      method: METHODS.GET,
      path: '/careprofessional/participants',
    },
    {
      method: METHODS.GET,
      path: '/participants',
    },
  ],
  name: 'CareProvider',

}];

export default roles;
