import accountSchema, { Account } from './persistence/account.schema';
import careProviderSchema, { CareProvider } from './persistence/care-provider.schema';
import organisationSchema, { Organisation } from './persistence/organisation.schema';
import participantSchema, { Participant } from './persistence/participant.schema';
import specialitySchema, { Speciality } from './persistence/speciality.schema';
import { ParticipantQuery } from './typings';

export const createParticipant = async (request: any): Promise<Participant> => {
  return (await participantSchema.create(request)).toObject();
};

export const findParticipant = async (query: ParticipantQuery) => {
  return participantSchema.findOne(query).lean();
};

export const findParticipants = async () => {
  return participantSchema.find().lean();
};

export const findParticipantByAddress = async (address: string): Promise<Participant> => {
  return participantSchema.findOne({ address }).lean();
};

export const findParticipantById = async (id: string): Promise<Participant> => {
  return participantSchema.findOne({ id }).lean();
};

export const findCareProviderByAddress = async (address: string): Promise<CareProvider> => {
  const careProvider = await careProviderSchema.findOne({ address }).lean();
  if (!careProvider) {
    return careProvider;
  }
  await populateCareProvider(careProvider);
  return careProvider;
};

const populateCareProvider = async (careProvider: any) => {
  const specialityFound = await findSpecialityById(careProvider.speciality);
  careProvider.speciality = specialityFound.name;
  const organisationFound = await findOrganisationById(careProvider.organisation);
  careProvider.organisation = organisationFound;
};

const findOrganisationById = async (id: string): Promise<Organisation> => {
  return organisationSchema.findById(id).populate(
    { path: 'locations', populate: 'address' },
  ).lean();
};

const findSpecialityById = async (id: string): Promise<Speciality> => {
  return specialitySchema.findById(id);
};

export const deleteParticipantByEmail = async (email: string): Promise<void> => {
  await participantSchema.findOneAndDelete({ email });
};

export const findUserByEmail = async (email: string): Promise<Account> => {
  return accountSchema.findOne({ email }).lean();
};

export const findUserByAddress = async (address: string): Promise<Account> => {
  return accountSchema.findOne({ address }).lean();
};
