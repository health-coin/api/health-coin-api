import 'mocha';
import * as Sinon from 'sinon';
import { server } from '../../../src/index';
import { LoginRequest } from '../../../src/modules/user/typings';
import * as UserController from '../../../src/modules/user/user.controller';
import * as UserRoute from '../../../src/modules/user/user.route';
import * as PersistenceUtils from '../persistence-utils';
import * as TestUtils from '../test-utils';
import InvalidPropertyTester from '../utils/InvalidPropertyTester';
import MissingPropertyTester from '../utils/MissingPropertyTester';
import * as ErrorChecks from '../validation/invalid/error-checks';
import invalidInput from '../validation/invalid/invalid-input';
import * as ValidBodyChecks from '../validation/valid/body-checks';
import validInput from '../validation/valid/valid-input';

const VALID_REQUEST: LoginRequest = {
  email: validInput.EMAIL_TJ,
  password: validInput.PASSWORD_TEST,
};

const EMAIL_PROPERTY = 'email';
const PASSWORD_PROPERTY = 'password';

const unauthorizedTester = new InvalidPropertyTester(
  VALID_REQUEST,
  UserRoute.loginUser,
  TestUtils.UNAUTHORIZED_MESSAGE,
  ErrorChecks.validateUnAuthorized,
);
const missingTester = new MissingPropertyTester(VALID_REQUEST, UserRoute.loginUser);

describe('Login user test', () => {
  before('Create a test account', async () => {
    Sinon.stub(UserController, 'createEthereumAccount').callsFake(async (password: string) => {
     return validInput.BLOCKCHAIN_ADDRESS_ONE;
    });

    await PersistenceUtils.deleteParticipant(VALID_REQUEST.email);
    const clone = TestUtils.createClone(validInput.PARTICIPANT_REQUEST);
    clone.email = VALID_REQUEST[EMAIL_PROPERTY];
    clone.password = VALID_REQUEST[PASSWORD_PROPERTY];
    await PersistenceUtils.createParticipant(clone);
  });

  after('Delete the test account', async () => {
    await PersistenceUtils.deleteParticipant(VALID_REQUEST[EMAIL_PROPERTY]);
    server.close();
  });

  describe(`Without ${EMAIL_PROPERTY}`, () => {
    missingTester.testProperty(EMAIL_PROPERTY);
  });

  describe(`With invalid ${EMAIL_PROPERTY}`, () => {
    unauthorizedTester.testProperty(EMAIL_PROPERTY, invalidInput.EMAIL_NO_AT);
  });

  describe(`Without ${PASSWORD_PROPERTY}`, () => {
    missingTester.testProperty(PASSWORD_PROPERTY);
  });

  describe(`With invalid ${PASSWORD_PROPERTY}`, () => {
    unauthorizedTester.testProperty(PASSWORD_PROPERTY, invalidInput.PASSWORD_CONTAINS_SPACES);
  });

  describe(`With valid ${EMAIL_PROPERTY} but wrong ${PASSWORD_PROPERTY}`, () => {
    unauthorizedTester.testProperty(PASSWORD_PROPERTY, validInput.PASSWORD_TEST2);
  });

  describe(`With valid ${PASSWORD_PROPERTY} but wrong ${EMAIL_PROPERTY}`, () => {
    unauthorizedTester.testProperty(EMAIL_PROPERTY, validInput.EMAIL);
  });

  describe('With a valid request', () => {
    it('Should return the user info', async () => {
      const result = await UserRoute.loginUser(TestUtils.createClone(VALID_REQUEST));
      ValidBodyChecks.validateSuccessResponse(result);
      ValidBodyChecks.validateAccount(result.data);
    });
  });
});
