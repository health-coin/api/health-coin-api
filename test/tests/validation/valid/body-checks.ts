import * as Chai from 'chai';
import { Role, Route } from '../../../../src/modules/auth/role/persistence/role.schema';
import { Token } from '../../../../src/modules/auth/token/persistence/token.schema';
import { DecodedToken } from '../../../../src/modules/auth/token/typings';
import { SuccessResponse } from '../../../../src/modules/transaction/typings';
import { Value } from '../../../../src/modules/treatment/persistence/measurement.schema';
import { Status } from '../../../../src/modules/treatment/persistence/status.schema';
import { TreatmentPlan } from '../../../../src/modules/treatment/persistence/treatment-plan.schema';
import { Account } from '../../../../src/modules/user/persistence/account.schema';
import { Address } from '../../../../src/modules/user/persistence/address.schema';
import { CareProvider } from '../../../../src/modules/user/persistence/care-provider.schema';
import { Location } from '../../../../src/modules/user/persistence/location.schema';
import { Organisation } from '../../../../src/modules/user/persistence/organisation.schema';
import { Participant } from '../../../../src/modules/user/persistence/participant.schema';
import { Person } from '../../../../src/modules/user/persistence/person.schema';

const should = Chai.should();

export const validateToken = (token: Token, hasExpDate: boolean = false) => {
  should.exist(token);
  should.exist(token.accessToken);
  should.exist(token.refreshToken);
  if (hasExpDate) {
    should.exist(token.expirationDate);
  }
};

const validateRoute = (route: Route) => {
  should.exist(route);
  should.exist(route.path);
  should.exist(route.method);
};

export const validateRole = (role: Role) => {
  should.exist(role);
  should.exist(role.name);
  should.exist(role.allowedRoutes);
  Array.isArray(role.allowedRoutes).should.equal(true);
  role.allowedRoutes.forEach(validateRoute);
};

export const validateDecodedToken = (decodedToken: DecodedToken) => {
  should.exist(decodedToken);
  should.exist(decodedToken.role);
  should.exist(decodedToken.address);
};

export const validateSuccessResponse = (response: SuccessResponse) => {
  should.exist(response.data);
  should.exist(response.message);
};

export const validateAccount = (account: Account) => {
  should.exist(account.address);
  should.exist(account.email);
  should.exist(account.registeredAt);
  should.exist(account.role);
  should.not.exist(account.password);
};

export const validatePerson = (person: Person) => {
  should.exist(person.dateOfBirth);
  should.exist(person.firstName);
  should.exist(person.lastName);
  should.exist(person.gender);
  should.exist(person.policyNumber);
};

export const validateParticipant = (participant: Participant) => {
  should.exist(participant.personalIdNumber);
  validatePerson(participant);
  validateAccount(participant);
};

export const validateCareProvider = (careProvider: CareProvider) => {
  should.exist(careProvider.firstName);
  should.exist(careProvider.lastName);
  should.exist(careProvider.gender);

  validateAccount(careProvider);
  should.exist(careProvider.speciality);
  should.exist(careProvider.phoneNumber);
  validateOrganisation(careProvider.organisation);
};

const validateOrganisation = (organisation: Organisation) => {
  should.exist(organisation);
  should.exist(organisation.name);
  should.exist(organisation.locations);
  organisation.locations.forEach(validateLocation);
};

const validateLocation = (location: Location) => {
  should.exist(location);
  should.exist(location.name);
  validateAddress(location.address);
};

const validateAddress = (address: Address) => {
  should.exist(address);
  should.exist(address.city);
  should.exist(address.country);
  should.exist(address.postalCode);
  should.exist(address.street);
  should.exist(address.streetNumber);
};

export const validateValues = (values: Value) => {
  should.exist(values.quantity);
  should.exist(values.unit);
};

export const validateStatus = (status: Status) => {
  should.exist(status.status);
};

export const validateTreatment = (treatment: TreatmentPlan) => {
  should.exist(treatment.endDate);
  should.exist(treatment.startDate);
};
