import * as Errors from 'restify-errors';
import {
  CreateParticipantRequest, GetCareProviderRequest, GetParticipantRequest, LoginRequest, PersonProperties,
} from './typings';
import * as ValidatorFuncs from './validator-funcs';

const validatePersonProperties = (request: PersonProperties) => {
  const DOB_PROPERTY = 'dateOfBirth';
  const FIRST_NAME_PROPERTY = 'firstName';
  const LAST_NAME_PROPERTY = 'lastName';
  const GENDER_PROPERTY = 'gender';
  const POLICY_NUMBER_PROPERTY = 'policyNumber';
  const { dateOfBirth, firstName, lastName, gender, policyNumber } = request;

  checkRequired(dateOfBirth, DOB_PROPERTY);
  checkRequired(firstName, FIRST_NAME_PROPERTY);
  checkRequired(lastName, LAST_NAME_PROPERTY);
  checkRequired(gender, GENDER_PROPERTY);
  checkRequired(policyNumber, POLICY_NUMBER_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidDateOfBirth(x), dateOfBirth, DOB_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidName(x), firstName, FIRST_NAME_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidName(x), lastName, LAST_NAME_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidGender(x), gender, GENDER_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidPolicyNumber(x), policyNumber, POLICY_NUMBER_PROPERTY);
};

export const validateCreateParticipantRequest = (request: CreateParticipantRequest) => {
  const EMAIL_PROPERTY = 'email';
  const PASSWORD_PROPERTY = 'password';
  const PERSONAL_ID_PROPERTY = 'personalIdNumber';
  const PERSON_PROPERTIES_PROPERTY = 'personProperties';

  const { email, password, personProperties, personalIdNumber } = request;
  checkRequired(personProperties, PERSON_PROPERTIES_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidObject(x), personProperties, PERSON_PROPERTIES_PROPERTY);
  validatePersonProperties(personProperties);

  checkRequired(email, EMAIL_PROPERTY);
  checkRequired(password, PASSWORD_PROPERTY);

  checkRequired(personalIdNumber, PERSONAL_ID_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidEmail(x), email, EMAIL_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidPassword(x), password, PASSWORD_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidPersonalNumber(x), personalIdNumber, PERSONAL_ID_PROPERTY);
};

export const validateLoginRequest = (request: LoginRequest) => {
  const EMAIL_PROPERTY = 'email';
  const PASSWORD_PROPERTY = 'password';
  const { email, password } = request;

  checkRequired(email, EMAIL_PROPERTY);
  checkRequired(password, PASSWORD_PROPERTY);

  const invalidMessage = `${password} of ${email} zijn ongeldig`;
  if (ValidatorFuncs.invalidEmail(email)) {
    throw new Errors.UnauthorizedError(invalidMessage);
  }
  if (ValidatorFuncs.invalidPassword(password)) {
    throw new Errors.UnauthorizedError(invalidMessage);
  }
};

export const validateGetUserRequest = (
  request: GetCareProviderRequest | GetParticipantRequest,
) => {
  const USER_ADDRESS_PROPERTY = 'userAddress';
  checkRequired(request.userAddress, USER_ADDRESS_PROPERTY);
};

const checkRequired = (property: any, propertyName: string) => {
  if (!property) {
    throw new Errors.BadRequestError(`${propertyName} is verplicht`);
  }
};

const checkValid = (
  isInvalidFunction: (property: any) => boolean, property: any, propertyName: string,
) => {
  if (isInvalidFunction(property)) {
    throw new Errors.BadRequestError(`${propertyName} is ongeldig`);
  }
};
