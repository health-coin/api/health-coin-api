import { Document, model, Model, Schema } from 'mongoose';
export const MEASUREMENT_MODEL_NAME = 'Measurement';

export const valueProperties = {
  quantity: {
    required: true,
    type: Number,
  },
  unit: {
    required: true,
    type: String,
  },
};

export const measurementSchema = new Schema({
  date: {
    default: Date.now(),
    required: false,
    type: Date,
  },
  values: [valueProperties],
});

export interface Measurement {
  date?: Date;
  values: Value[];
}

export interface Value {
  quantity: number;
  unit: string;
}

type ModelType = Value & Document;
let measurement: Model<ModelType>;
try {
  measurement = model<ModelType>(MEASUREMENT_MODEL_NAME);
} catch (err) {
  measurement = model<ModelType>(MEASUREMENT_MODEL_NAME, measurementSchema);
}
export default measurement;
