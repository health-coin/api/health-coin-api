# Health Coin API

[![coverage report](https://gitlab.com/health-coin/api/health-coin-api/badges/coverage/coverage.svg)](https://gitlab.com/health-coin/api/health-coin-api/commits/coverage)

# To get started
1. Make sure you have NodeJS installed.
2. Install typescript with npm i typescript -g
3. Cd into the root of the project and install all dependencies with npm i
4. Download MongoDB
5. Install the TSLint extension
6. Run npm start and npm test to see if everything is working correctly

# Project Architecture
index.ts is the entrypoint of the application. It loads all high level components like MongoDB, Web3 and the API routes (main-rest.bridge).
The main-rest.bridge file contains all the API routes and forwards the request to the main.route. 
The main.route formats the request and sends the expected request object to the main.controller.
The main-rest-bridge and main-route should be the only files depending on Restify, we would be able to change the communication protol or library this way.
the main.controller handles the integration between the modules (see modules directory).

The application is divided into 5 modules, these modules have high cohesion and should not depend or know about the other modules. 
The auth module: responsible for authorization, it handles access tokens and roles.
The notification module: responsible for sending notification like emails and Firebase.
The transaction module: handles Healthcoin transactions.
The treatment module: knows about the treatmentplans and measurements.
The user module: knows about the users of the system like the careproviders and participants.

Most modules consist of the following files:
.route files: responsible for validating the request and forwarding the request to the controller.
.validator files: contains all validation checks.
.typings files: contain all TypeScript type interfaces.
.controller files: handles the domain logic, makes often use of the .service file.
.service files: talks to mongoose schemas, the service file could be replaced to use different database technology. The service files should not handle any logic and throw errors, this is the responsibility of the controller file.
.schema files: mongoose schemas to interface with MongoDB.

# Tests
Tests can be executed with the npm test command, this will run the .test files in the test directory. 
The test files use the invalid and valid input files to reuse test input. 
The body-checks file functions can be reused to validate responses.
The persistence-utils file can be used to quickly create test objects. 
test-utils.ts contains some test helper functions and messages. 
It's important to realise that the test files shouldn't direct depend on Mongo or REST, it would be hard to replace these components otherwise. 

# Swagger
The API documentation is written with swagger. It shows all requests with expected input and output. 
You can start the server and access the documentation on yourhost:port/docs/
The swagger documentation files can be found in the docs/files directory. 

# Truffle
Truffle is used to deploy smart contracts. Ganache can be used to run a local Ethereum blockchain.
Install truffle with: npm i truffle -g
Compile contracts with: truffle compile 
Deploy contracts with: truffle migrate
Edit truffle-config.js to deploy to a different server
