import * as ErrorChecks from './validation/invalid/error-checks';

export const BAD_REQUEST_MESSAGE = 'Should return a bad request';
export const NOT_FOUND_MESSAGE = 'Should return a bad request';
export const CONFLICT_MESSAGE = 'Should return a conflict error';
export const UNAUTHORIZED_MESSAGE = 'Should return unauthorized';
export const FORBIDDEN_MESSAGE = 'Should return forbidden';
export const SCHEME_VALIDATION_MESSAGE = 'Should return a mongoDB error';

export const createClone = (object: any) => {
  return JSON.parse(JSON.stringify(object));
};

export const executeAndValidateError = async (
  executePromise: any,
  errorFunction: (error: ErrorChecks.Error) => any,
  errorMessage: string = 'No error was thrown',
) => {
  let exists = false;
  try {
    await executePromise;
    exists = true;
  } catch (err) {
    errorFunction(err);
  }
  if (exists) {
    throw new Error(errorMessage);
  }
};
