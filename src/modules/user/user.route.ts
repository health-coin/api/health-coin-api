import mainConfig from '../../../config/main.config';
import { createOrFindCareProviders } from './careproviders';
import participants from './participants';
import {
  CreateParticipantRequest, GetCareProviderRequest, GetParticipantByIdRequest, GetParticipantRequest, LoginRequest, SuccessResponse,
} from './typings';
import * as UserController from './user.controller';
import * as UserValidator from './user.validator';

export const createParticipant = async (
  request: CreateParticipantRequest,
): Promise<SuccessResponse> => {
  UserValidator.validateCreateParticipantRequest(request);
  return UserController.createParticipant(request);
};

export const loginUser = async (request: LoginRequest): Promise<SuccessResponse> => {
  UserValidator.validateLoginRequest(request);
  return UserController.loginUser(request);
};

export const getCareProvider = async (request: GetCareProviderRequest) => {
  UserValidator.validateGetUserRequest(request);
  return UserController.getCareProvider(request);
};

export const getParticipant = async (request: GetParticipantRequest) => {
  UserValidator.validateGetUserRequest(request);
  return UserController.getParticipant(request);
};

export const getParticipants = async () => {
  return UserController.getParticipants();
};

export const getParticipantById = async (request: GetParticipantByIdRequest) => {
  return UserController.getParticipantById(request);
};

export const getAllParticipants = async () => {
  return UserController.getParticipants();
};

const loadTestUsers = async () => {
  if (isInTestEnvironment()) {
    return;
  }
  const insertUser = async (participant: CreateParticipantRequest) => {
    try {
      await createParticipant(participant);
    } catch (err) {
      console.log(`Not inserting ${participant.email} already exists.`);
    }
  };
  await Promise.all(participants.map(account => insertUser(account)));
  await createOrFindCareProviders();
};

const isInTestEnvironment = () => {
  return mainConfig.environment.toLowerCase() === 'test';
};

(async () => await loadTestUsers())();
