import { Participant } from './persistence/participant.schema';
import { SuccessResponse } from './typings';

export interface SuccessResponse {
  message: string;
  data: any;
}

export interface ParticipantQuery {
  address?: string;
  email?: string;
  personalIdNumber?: string;
  policyNumber?: string;
  _id?: string;
}

export interface PersonProperties {
  dateOfBirth: string;
  firstName: string;
  gender: string;
  lastName: string;
  policyNumber: string;
}

export interface CreateParticipantRequest {
  address?: string;
  email: string;
  password: string;
  personProperties: PersonProperties;
  personalIdNumber: string;
}

export interface GetCareProviderRequest {
  userAddress: string;
}
export interface GetParticipantRequest {
  userAddress: string;
}

export interface GetParticipantByIdRequest {
  userId: string;
}

export interface SuccesfullyAddedUser extends SuccessResponse {
  data: Participant;
}

export interface LoginRequest {
  email: string;
  password: string;
}
