import * as MainController from '../../src/main.controller';
import { Role, Route } from '../../src/modules/auth/role/persistence/role.schema';
import * as RoleRoute from '../../src/modules/auth/role/role.route';
import { Token } from '../../src/modules/auth/token/persistence/token.schema';
import * as TokenRoute from '../../src/modules/auth/token/token.route';
import treatmentPlanSchema from '../../src/modules/treatment/persistence/treatment-plan.schema';
import addressSchema from '../../src/modules/user/persistence/address.schema';
import careProviderSchema from '../../src/modules/user/persistence/care-provider.schema';
import locationSchema from '../../src/modules/user/persistence/location.schema';
import organisationSchema from '../../src/modules/user/persistence/organisation.schema';
import specialitySchema from '../../src/modules/user/persistence/speciality.schema';
import { CreateParticipantRequest } from '../../src/modules/user/typings';
import * as UserController from '../../src/modules/user/user.controller';
import * as UserRoute from '../../src/modules/user/user.route';
import * as UserService from '../../src/modules/user/user.service';
import { createClone } from './test-utils';
import validInput from './validation/valid/valid-input';

export const findTokenById = async (id: string) => {
  return TokenRoute.findTokenById({ id });
};

export const createToken = async (
  address: string = validInput.ADDRESS, role: string = validInput.ROLE_PARTICIPANT,
): Promise<Token> => {
  return TokenRoute.createToken({ address, role });
};

export const deleteToken = async (accessToken: string) => {
  await TokenRoute.revokeToken({ accessToken });
};

export const getOrCreateRole = async (
  name: string = validInput.ROLE_TEST,
  allowedRoutes: Route[] = [{ path: validInput.PATH_ME, method: validInput.METHOD_GET }],
): Promise<Role> => {
  try {
    const roleFound = await RoleRoute.findRoleByName({ name });
    return roleFound;
  } catch (err) {
    return RoleRoute.createRole({ name, allowedRoutes });
  }
};

export const deleteRole = async (name: string) => {
  await RoleRoute.deleteRole({ name });
};

export const findRoleByName = async (name: string): Promise<Role> => {
  return RoleRoute.findRoleByName({ name });
};

export const createParticipant = async (
  participant: CreateParticipantRequest = validInput.PARTICIPANT_REQUEST,
) => {
  return (await UserRoute.createParticipant(createClone(participant))).data;
};

export const deleteParticipant = async (email: string): Promise<void> => {
  return UserService.deleteParticipantByEmail(email);
};

export const createOrFindCareProvider = async (
  careProviderProperties = validInput.CARE_PROVIDER_REQUEST,
  personProperties = validInput.PERSON_PROPERTIES,
  organisationName = validInput.ORGANISATION_NAME,
) => {
  const careProviderFound = await careProviderSchema.findOne(
    { email: careProviderProperties.email },
  ).lean();
  if (careProviderFound) {
    return careProviderFound;
  }

  const carePropertiesClone = createClone(careProviderProperties);
  const personPropertiesClone = createClone(personProperties);
  const speciality = await createOrFindSpeciality(carePropertiesClone.speciality);
  const organisation = await createOrFindOrganisation(organisationName);

  const blockChainAddress = await UserController.createEthereumAccount(
    carePropertiesClone.password,
  );
  personPropertiesClone.address = blockChainAddress;
  const hashedPassword = await UserController.hashPassword(carePropertiesClone.password);
  carePropertiesClone.password = hashedPassword;

  return (await careProviderSchema.create({
     ...carePropertiesClone,
     ...personPropertiesClone,
     ...{ organisation: organisation._id, speciality: speciality._id },
  })).toObject();
};

const createOrFindSpeciality = async (name: string = validInput.SPECIALITY) => {
  const specialityFound = await specialitySchema.findOne({ name }).lean();
  if (specialityFound) {
    return specialityFound;
  }
  return (await specialitySchema.create({ name })).toObject();
};

const createOrFindOrganisation = async (
  name: string = validInput.ORGANISATION_NAME,
  locationNames: string[] = [validInput.LOCATION_NAME],
) => {
  const organisationFound = await organisationSchema.findOne({ name }).lean();
  if (organisationFound) {
    return organisationFound;
  }
  const locations = await Promise.all(
    locationNames.map(locationName => createOrFindLocation(locationName),
  ));
  const locationIds = locations.map(location => location._id);
  return organisationSchema.create({ locations: locationIds, name });
};

const createOrFindLocation = async(
  name: string = validInput.LOCATION_NAME,
  addressValues = validInput.ADDRESS_REQUEST,
) => {
  const locationFound = await locationSchema.findOne({ name }).lean();
  if (locationFound) {
    return locationFound;
  }
  const address = await createOrFindAddress(addressValues);
  return (await locationSchema.create({ address: address._id, name })).toObject();
};

const createOrFindAddress = async (addressValues = validInput.ADDRESS_REQUEST) => {
  const addressFound = await addressSchema.findOne(addressValues).lean();
  if (addressFound) {
    return addressFound;
  }
  return (await addressSchema.create(addressValues)).toObject();
};

export const createTreatmentPlan = async (
  careProviderId: string,
  participantId: string,
  customMeasurements = [],
  contractAddress: string = validInput.BLOCKCHAIN_ADDRESS_THREE,
) => {
  return (await treatmentPlanSchema.create(
    { careProviderId, participantId, customMeasurements, contractAddress },
  )).toObject();
};

export const deleteTreatmentPlanByParticipants = async (
  careProviderId: string, participantId: string,
) => {
  await treatmentPlanSchema.findOneAndDelete({ careProviderId, participantId });
};

export const deleteTreatmentPlanById = async (id: string) => {
  await treatmentPlanSchema.findByIdAndDelete(id);
};

export const createAccessToken = async (email: string, password: string) => {
  const result = await MainController.loginUser({ email, password });
  return result.data.token.accessToken;
};
