import { BAD_REQUEST_MESSAGE, createClone } from '../test-utils';
import * as ErrorChecks from '../validation/invalid/error-checks';
import PropertyTester from './PropertyTester';

export default class InvalidPropertyTester extends PropertyTester {

  public constructor(
    validObject: any,
    routeFunction: (request: any) => Promise<any>,
    message: string = BAD_REQUEST_MESSAGE,
    errorCheckFunction = ErrorChecks.validateBadRequest,
  ) {
    super(validObject, routeFunction, message, errorCheckFunction);
  }

  public testProperty(
      propertyName: string,
      property: any,
      setPropertyFunction: (
          validObject: any, propertyName: string, property: any,
      ) => void = setPropertyFunc,
    ) {
    it(this.message, async () => {
      const clone = createClone(this.validObject);
      setPropertyFunction(clone, propertyName, property);
      this.checkCloneEquals(clone, propertyName);

      let exists = false;
      try {
        await this.routeFunction(clone);
        exists = true;
      } catch (err) {
        this.errorCheckFunction(err.body);
      }
      if (exists) {
        throwExistsError(this.validObject, propertyName);
      }
    });
  }
}

const setPropertyFunc = (validObject: any, propertyName: string, property: any): void => {
  validObject[propertyName] = property;
};

const throwExistsError = (property: any, propertyName: string) => {
  throw new Error(`Route returns a result for propery: ${property} with
    name: ${propertyName}`,
  );
};
