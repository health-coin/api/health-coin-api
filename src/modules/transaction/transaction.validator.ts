import * as Errors from 'restify-errors';
import { web3 } from '../../index';
import { BalanceQuery, TransactionRequest } from './typings';

export const validateBalanceRequest = async (query: BalanceQuery) => {
  const ID_PARAM = 'id parameter';
  checkRequired(query.id, ID_PARAM);
  checkValid(address => !web3.utils.isAddress(address), query.id, ID_PARAM);
};

export const validateTransferRequest = async (query: TransactionRequest) => {
  const ADDRESS_TO = 'adres naar';
  const ADDRESS_FROM = 'adres van';
  const AMOUNT = 'hoeveelheid';

  checkRequired(query.to, ADDRESS_TO);
  checkRequired(query.from, ADDRESS_FROM);
  checkRequired(query.amount, AMOUNT);
  checkValid(address => !web3.utils.isAddress(address), query.to, ADDRESS_TO);
  checkValid(address => !web3.utils.isAddress(address), query.from, ADDRESS_FROM);
  checkValid(
    amount => isNaN(amount) || amount <= 0 || !isInt(amount),
    query.amount, AMOUNT,
  );
};

const isInt = (n: number) => {
  return n % 1 === 0;
};

const checkRequired = (property: any, propertyName: string) => {
  if (!property) {
    throw new Errors.BadRequestError(`${propertyName} is verplicht`);
  }
};

const checkValid = (
  isInvalidFunction: (property: any) => boolean, property: any, propertyName: string,
) => {
  if (isInvalidFunction(property)) {
    throw new Errors.BadRequestError(`${propertyName} is ongeldig`);
  }
};
