import * as Chai from 'chai';
import * as Sinon from 'sinon';
import { server } from '../../../src/index';
import { GetCareProviderRequest } from '../../../src/main-typings';
import * as MainController from '../../../src/main.controller';
import { TreatmentPlan } from '../../../src/modules/treatment/persistence/treatment-plan.schema';
// tslint:disable-next-line: max-line-length
import * as TreatmentContractService from '../../../src/modules/treatment/treatment-contract.service';
import { TreatmentPlanContractRequest } from '../../../src/modules/treatment/typings';
import { CareProvider } from '../../../src/modules/user/persistence/care-provider.schema';
import { Participant } from '../../../src/modules/user/persistence/participant.schema';
import * as UserController from '../../../src/modules/user/user.controller';
import * as PersistenceUtils from '../persistence-utils';
import * as TestUtils from '../test-utils';
import * as ErrorChecks from '../validation/invalid/error-checks';
import * as ValidBodyChecks from '../validation/valid/body-checks';
import validInput from '../validation/valid/valid-input';

const VALID_REQUEST: GetCareProviderRequest = TestUtils.createClone({
    accessToken: validInput.ACCESS_TOKEN,
    careProviderAddress: validInput.BLOCKCHAIN_ADDRESS_THREE,
});

const should = Chai.should();

describe('Get careprovider info integration tests', () => {
  let participant: Participant;
  let participantAccessToken;
  let careProvider: CareProvider;

  before('Create test objects', async () => {
    Sinon.stub(UserController, 'createEthereumAccount').callsFake(async (password: string) => {
      return validInput.BLOCKCHAIN_ADDRESS_TWO;
    });
    Sinon.stub(TreatmentContractService, 'generateDocumentHash')
      .callsFake(async (treatment: TreatmentPlan) => {
        return validInput.BLOCKCHAIN_ADDRESS_TWO;
    });
    Sinon.stub(TreatmentContractService, 'getTreatmentData')
      .callsFake(async (req: TreatmentPlanContractRequest) => {
        return {} as any;
    });

    participant = await PersistenceUtils.createParticipant(validInput.PARTICIPANT_REQUEST);
    careProvider = await PersistenceUtils.createOrFindCareProvider();

    participantAccessToken = await PersistenceUtils.createAccessToken(
        validInput.PARTICIPANT_REQUEST.email, validInput.PARTICIPANT_REQUEST.password,
    );
    VALID_REQUEST.accessToken = participantAccessToken;
    VALID_REQUEST.careProviderAddress = careProvider.address;
  });

  after('Delete test objects', async () => {
    Sinon.restore();
        server.close();
    await PersistenceUtils.deleteParticipant(participant.email);
  });

  describe('When the participant does not have a treatmentplan with the careprovider', () => {
    before('Make sure treatmentplan does not exist', async () => {
      await PersistenceUtils.deleteTreatmentPlanByParticipants(careProvider._id, participant._id);
    });

    it(TestUtils.FORBIDDEN_MESSAGE, async () => {
      await TestUtils.executeAndValidateError(
        MainController.getCareProvider(VALID_REQUEST),
        ErrorChecks.validateForbidden,
      );
    });
  });

  describe('When the participant has a treatmentplan with the careprovider', () => {
    let treatmentPlan: TreatmentPlan;

    before('Create the treatment plan', async () => {
      treatmentPlan = await PersistenceUtils.createTreatmentPlan(careProvider._id, participant._id);
    });

    after('Delete the treatment plan', async () => {
      await PersistenceUtils.deleteTreatmentPlanById(treatmentPlan._id);
    });

    it('Should return info of the careprovider', async () => {
      const result = await MainController.getCareProvider(VALID_REQUEST);
      ValidBodyChecks.validateSuccessResponse(result);
      const returnedCareProvider = result.data;
      ValidBodyChecks.validateCareProvider(returnedCareProvider);
      should.not.exist(returnedCareProvider.dateOfBirth);
      should.not.exist(returnedCareProvider.policyNumber);
    });
  });
});
