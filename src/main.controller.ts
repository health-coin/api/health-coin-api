import * as Errors from 'restify-errors';
import {
  AccessTokenRequest,
  CreateParticipantRequest, CreateTransactionRequest,
  GetCareProviderRequest, GetParticipantRequest, GetUserBalanceRequest, HasRightsRequest, LoginRequest, SuccessResponse,
} from './main-typings';
import * as RoleRoute from './modules/auth/role/role.route';
import * as TokenRoute from './modules/auth/token/token.route';
import * as TransactionRoute from './modules/transaction/transaction.route';
import treatmentPlanSchema, { TreatmentPlan } from './modules/treatment/persistence/treatment-plan.schema';
import * as TreatmentRoute from './modules/treatment/treatment.route';
import {
  AddMeasurementRequest, CreateTreatmentplanRequest, GetTreatmentPlansRequest,
  TreatmentActiveRequest, TreatmentByIdRequest,
} from './modules/treatment/typings';
import { CareProvider } from './modules/user/persistence/care-provider.schema';
import { Participant } from './modules/user/persistence/participant.schema';
import * as UserRoute from './modules/user/user.route';
import { RewardTransactionQuery } from './modules/transaction/typings';

/*
  This file handles the integration between modules (can call multiple route files)
*/

export const getBalance = async (request: any) => {
  return TransactionRoute.getBalance();
};

export const getUserBalance = async (request: GetUserBalanceRequest) => {
  const decodedToken = await TokenRoute.decodeToken(request);
  return TransactionRoute.getUserBalance({ id: decodedToken.address });
};

export const rewardHealthcoins = async (request: RewardTransactionQuery) => {
  return TransactionRoute.rewardHealthcoins(request);
};

export const createTransaction = async (request: CreateTransactionRequest) => {
  const decodedToken = await TokenRoute.decodeToken(request);
  return TransactionRoute.createTransaction({
    amount: request.amount,
    from: decodedToken.address,
    to: request.to,
  });
};

export const loginUser = async (request: LoginRequest): Promise<SuccessResponse> => {
  const userResponse = await UserRoute.loginUser(request);
  const user = userResponse.data;
  const token = await TokenRoute.createToken({ role: user.role, address: user.address });
  return { message: userResponse.message, data: { ...user, ...{ token } } };
};

export const revokeToken = async (request: any) => {
  return TokenRoute.revokeToken(request);
};

export const refreshToken = async (request: any) => {
  return TokenRoute.refreshToken(request);
};

export const hasRights = async (request: HasRightsRequest): Promise<void> => {
  const decodedToken = await TokenRoute.decodeToken(request);
  await RoleRoute.hasRights({ role: decodedToken.role, route: request.route });
};

export const createParticipant = async (request: CreateParticipantRequest) => {
  return UserRoute.createParticipant(request);
};

export const getAllParticipants = async () => {
  return UserRoute.getParticipants();
};

export const getParticipant = async (request: GetParticipantRequest) => {
  const treatmentPlans = (await findTreatmentPlans(request)).data;
  const participant = (await UserRoute.getParticipant(
    { userAddress: request.participantAddress },
  )).data;
  const filteredPlans = filterPlansOnParticipantId(treatmentPlans, participant._id);
  if (!filteredPlans || filteredPlans.length <= 0) {
    throw new Errors.ForbiddenError('Je heb geen behandelplan met de deelnemer.');
  }

  return {
    data: participant,
    message: 'Deelnemer succesvol opgehaald',
  };
};

export const getParticipants = async (request: AccessTokenRequest) => {
  const treatmentPlans = (await findTreatmentPlans(request)).data;
  const participants = await getParticipantsFromTreatmentplans(treatmentPlans);
  const uniqueParticipants = [...new Map(participants.map(item => [item.address, item])).values()];
  return {
    data: uniqueParticipants,
    message: 'Deelnemers die bij zorgspecialist horen zijn succesvol opgehaald',
  };
};

const getParticipantsFromTreatmentplans = async (treatmentPlans) => {
  const participants: Participant[] = [];
  const promises = await treatmentPlans.map(async (treatmentPlan) => {
    const { participantId } = treatmentPlan.treatment;
    const participant = (await UserRoute.getParticipantById(
      { userId: participantId },
    )).data;
    participants.push(participant);
  });
  await Promise.all(promises);
  return participants;
};

export const getCareProvider = async (request: GetCareProviderRequest) => {
  const treatmentPlans = (await findTreatmentPlans(request)).data;
  const careProvider = (await UserRoute.getCareProvider(
    { userAddress: request.careProviderAddress },
  )).data;
  const filteredPlans = filterPlansOnCareProviderId(treatmentPlans, careProvider._id);
  if (!filteredPlans || filteredPlans.length <= 0) {
    throw new Errors.ForbiddenError('Je heb geen behandelplan met de zorgprofessional.');
  }
  removeSensitiveCareProviderProperties(careProvider);

  return {
    data: careProvider,
    message: 'Zorgprofessional succesvol opgehaald',
  };
};

const removeSensitiveCareProviderProperties = (careProvider: CareProvider) => {
  delete careProvider.policyNumber;
  delete careProvider.dateOfBirth;
};

const filterPlansOnCareProviderId = (plans: any[], id: string) => {
  const careProviderId = id.toString();
  return plans.filter(plan => plan.treatment.careProviderId.toString() === careProviderId);
};
const filterPlansOnParticipantId = (plans: any[], id: string) => {
  const participantId = id.toString();
  return plans.filter(plan => plan.treatment.participantId.toString() === participantId);
};

export const createTreatmentplan = async (request: CreateTreatmentplanRequest) => {
  const decodedToken = await TokenRoute.decodeToken(request);
  request.careProviderAddress = decodedToken.address;
  return TreatmentRoute.createTreatmentplan(request);
};

export const findTreatmentPlans = async (request: GetTreatmentPlansRequest) => {
  const decodedToken = await TokenRoute.decodeToken(request);
  request.senderAddress = decodedToken.address;
  return TreatmentRoute.getTreatmentPlans(request);
};

export const findTreatmentplanById = async (request: TreatmentByIdRequest) => {
  const decodedToken = await TokenRoute.decodeToken(request);
  request.senderAddress = decodedToken.address;
  return TreatmentRoute.getTreatmentplanById(request);
};

export const acceptTreatmentPlan = async (request: TreatmentByIdRequest) => {
  const decodedToken = await TokenRoute.decodeToken(request);
  request.senderAddress = decodedToken.address;
  return TreatmentRoute.acceptTreatmentplan(request);
};

export const findActiveTreatmentplan = async (request: TreatmentActiveRequest) => {
  const decodedToken = await TokenRoute.decodeToken(request);
  request.senderAddress = decodedToken.address;
  return TreatmentRoute.getActiveTreatmentplan(request);
};

export const addMeasurement = async (request: AddMeasurementRequest) => {
  const decodeToken = await TokenRoute.decodeToken(request.treatmentData);
  request.treatmentData.senderAddress = decodeToken.address;
  return TreatmentRoute.addMeasurement(request);
};
