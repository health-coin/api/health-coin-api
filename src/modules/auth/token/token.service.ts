import tokenSchema, { Token } from './persistence/token.schema';

export const findTokenById = async (id: string): Promise<Token> => {
  return tokenSchema.findById(id).lean();
};

export const deleteTokenById = async (id: string) => {
  await tokenSchema.findByIdAndDelete(id);
};

export const findTokenByAccessToken = async (accessToken: string): Promise<Token> => {
  return tokenSchema.findOne({ accessToken }).lean();
};

export const findTokenByRefreshToken = async (refreshToken: string): Promise<Token> => {
  return tokenSchema.findOne({ refreshToken }).lean();
};

export const createToken = async (accessToken: string, refreshToken: string): Promise<Token> => {
  return (await tokenSchema.create({ accessToken, refreshToken })).toObject();
};
