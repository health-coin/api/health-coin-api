import { VariantAlsoNegotiatesError } from 'restify-errors';
import { CreateParticipantRequest } from '../../../../src/main-typings';
import { CreateTreatmentplanRequest } from '../../../../src/modules/treatment/typings';

const vars = {
  ACCESS_TOKEN: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZGRyZXNzIjoiNWNh
  NGVjNzlkZWQ4NjAxYzU4Mzg5N2Y5Iiwicm9sZSI6IlBhcnRpY2lwYW50IiwiZXhwIjoxNTU0
  ODExMDUzNjU1LCJpYXQiOjE1NTQ4MDM4NTN9.NiIy77gk6vqRCHmu0w8LMrfyHkEZSeaNSKAM68KDqi0`,
  ADDRESS: '0x0EdEd6679670C151F1033598e1f20Ec424AcA39B',
  AMOUNT_ONE: 1,
  ID: '5ca4ec79ded8601c583897f9',
  METHOD_DELETE: 'DELETE',
  METHOD_GET: 'GET',
  REFRESH_TOKEN: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZGRyZXNzIjoiNWNhNGVjNzl
  kZWQ4NjAxYzU4Mzg5N2Y5Iiwicm9sZSI6IlBhcnRpY2lwYW50IiwiY3JlYXRlZCI6IjIwMTktMDQtMDl
  UMTA6MDM6NDYuNzgwWiIsImlhdCI6MTU1NDgwNDIyNn0.TFqy_gtCS_T1FFf2yRKgiEi-HLms_99JCUkIYx7MaeU`,
  ROLE_ADMIN: 'Admin',
  ROLE_PARTICIPANT: 'Participant',
  ROLE_TEST: 'Test',

  PATH_BALANCE: '/balance',
  PATH_ME: '/me',

  BLOCKCHAIN_ADDRESS_ONE: '0xA41e844d63e839acc66AC7c9da852AfdeB9a88DF',
  BLOCKCHAIN_ADDRESS_THREE: '0x423451E5a23F1B6902C0C8A4045F0FD4F4276C08',
  BLOCKCHAIN_ADDRESS_TWO: '0xE6b156f456253C9Ad76d62212Fc4317fb786FbC6',

  DATE_OF_BIRTH: '2000-09-14',
  EMAIL: 'henkie@test.com',
  EMAIL_PROF: 'careProf@gmail.com',
  EMAIL_TJ: 'tjvanderende@test.com',
  FIRST_NAME_HENK: 'Henk',
  GENDER_MALE: '2',
  LAST_NAME_TEST: 'Test',
  PASSWORD_TEST: 'Testing1',
  PASSWORD_TEST2: 'Testing2',
  PERSONAL_ID_NUMBER: '383838383',
  PERSONAL_ID_NUMBER2: '283838383',
  POLICY_NUMBER: '32333233',
  POLICY_NUMBER2: '222333233',

  LOCATION_NAME: 'Zorgwoning Hertenwei',
  ORGANISATION_NAME: 'Vivantes',
  PHONE_NUMBER: '0602194569',
  SPECIALITY: 'Diëtist',

  ADDITION: 'a',
  CITY: 'Utrecht',
  COUNTRY: 'Nederland',
  POSTALCODE: '6162KZ',
  STREET: 'Om de Toren',
  STREET_NUMBER: '90',

  COINREWARD: 258,
  ENDDATE: '2019-08-15',
  MEASUREMENT_QUANTITY: 35,
  MEASUREMENT_UNIT: 'BMI',
};

const ADDRESS_REQUEST = {
  addition: vars.ADDITION,
  city: vars.CITY,
  country: vars.COUNTRY,
  postalCode: vars.POSTALCODE,
  street: vars.STREET,
  streetNumber: vars.STREET_NUMBER,
};

const PERSON_PROPERTIES = {
  dateOfBirth: vars.DATE_OF_BIRTH,
  firstName: vars.FIRST_NAME_HENK,
  gender: vars.GENDER_MALE,
  lastName: vars.LAST_NAME_TEST,
  policyNumber: vars.POLICY_NUMBER,
};

const PARTICIPANT_REQUEST: CreateParticipantRequest = {
  email: vars.EMAIL_TJ,
  password: vars.PASSWORD_TEST,
  personProperties: PERSON_PROPERTIES,
  personalIdNumber: vars.PERSONAL_ID_NUMBER,
};

const CARE_PROVIDER_REQUEST = {
  email: vars.EMAIL_PROF,
  password: vars.PASSWORD_TEST,
  phoneNumber: vars.PHONE_NUMBER,
};

const TREATMENT_REQUEST: CreateTreatmentplanRequest = {
  accessToken: vars.ACCESS_TOKEN,
  coinReward: vars.COINREWARD,
  endDate: vars.ENDDATE,
  measurements: [{
    quantity: vars.MEASUREMENT_QUANTITY,
    unit: vars.MEASUREMENT_UNIT,
  }],
  participantId: vars.ID,
};

export default {
  ...vars,
  ...{ PARTICIPANT_REQUEST },
  ...{ TREATMENT_REQUEST },
  ...{ ADDRESS_REQUEST },
  ...{ PERSON_PROPERTIES },
  ...{ CARE_PROVIDER_REQUEST },
};
