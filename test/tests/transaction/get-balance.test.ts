import 'mocha';
import { server } from '../../../src/index';
import * as MainController from '../../../src/main.controller';
import InvalidPropertyTester from '../utils/InvalidPropertyTester';
import MissingPropertyTester from '../utils/MissingPropertyTester';
import invalidInput from '../validation/invalid/invalid-input';
import validInput from '../validation/valid/valid-input';

const VALID_REQUEST = {
  id: validInput.ADDRESS,
};

const ID_PROPERTY = 'id';
const invalidTester = new InvalidPropertyTester(VALID_REQUEST, MainController.getUserBalance);
const missingTester = new MissingPropertyTester(VALID_REQUEST, MainController.getUserBalance);

describe('Get user balance test', () => {
  after('Close server', () => {
    server.close();
  });

  describe(`Without ${ID_PROPERTY}`, () => {
    missingTester.testProperty(ID_PROPERTY);
  });

  describe(`With invalid ${ID_PROPERTY}`, () => {
    invalidTester.testProperty(ID_PROPERTY, invalidInput.ADDRESS_CONTAINS_SYMBOL);
  });
});
