import { Document, model, Model, Schema } from 'mongoose';

export const STATUS_MODEL_NAME = 'Status';

export const statusPoperties = {
  status: {
    required: true,
    type: String,
  },
};

const statusSchema = new Schema(statusPoperties);

export interface Status {
  status: string;
}

type ModelType = Status & Document;
let status: Model<ModelType>;
try {
  status = model<ModelType>(STATUS_MODEL_NAME);
} catch (err) {
  status = model<ModelType>(STATUS_MODEL_NAME, statusSchema);
}
export default status;
