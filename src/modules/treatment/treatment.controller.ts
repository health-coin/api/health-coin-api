import * as Errors from 'restify-errors';
import { web3 } from '../..';
import Role from '../user/Role';
import * as UserService from '../user/user.service';
import { Measurement } from './persistence/measurement.schema';
import { TreatmentPlan } from './persistence/treatment-plan.schema';
import * as TreatmentContractService from './treatment-contract.service';
import * as TreatmentService from './treatment.service';
import {
  AddMeasurementRequest,
  CreateTreatmentContractRequest,
  CreateTreatmentplanRequest,
  GetTreatmentPlansRequest,
  SuccesfullyReceivedTreatmentPlan,
  SuccessResponse,
  TreatmentActiveRequest,
  TreatmentByIdRequest,
  TreatmentPlanContract,
  TreatmentPlanContractMeasurement,
  TreatmentPlanContractRequest,
  TreatmentPlanContractState,
} from './typings';

const blockchainErrorToReasons = (errorObject) => {
  const errorMessage = errorObject.message;
  const message = errorMessage.split('Node error: ')[1];
  if (message === undefined) {
    throw errorObject;
  } else {
    const parsedMessage = JSON.parse(message);
    errorObject.message = parsedMessage.message;
  }
};

export const createTreatmentPlan = async (
  request: CreateTreatmentplanRequest,
): Promise<SuccessResponse> => {
  try {
    const { careProviderAddress, participantId, endDate, coinReward, measurements } = request;
    const participant = await UserService.findParticipant({ _id: participantId });
    if (!participant) {
      throw new Errors.BadRequestError(`Deelnemer ${participantId} bestaat niet`);
    }

    const careProvider = await UserService.findCareProviderByAddress(careProviderAddress);
    const participantAddress = participant.address;
    request.careProviderId = careProvider._id.toString();

    const treatment = await TreatmentService.createTreatmentplan({
      ...request,
      ...{ contractAddress: null }, // contract isn't generated yet, so fill in null.
    });

    const documentHash = await TreatmentContractService.generateDocumentHash(treatment);
    const endDateToTimestamp = new Date(endDate).getTime();
    const createTreatmentContractRequest: CreateTreatmentContractRequest = {
      careProviderAddress,
      coinReward,
      documentHash,
      measurements,
      participantAddress,
      plannedEndDate: endDateToTimestamp,
    };
    const treatmentContract = await TreatmentContractService.createTreatmentContract(
      createTreatmentContractRequest,
    );

    await TreatmentService.updateTreatmentContractAddress(
      treatment._id,
      treatmentContract,
    );

    return {
      data: treatment,
      message: 'Behandelplan aangemaakt.',
    };
  } catch (erx) {
    blockchainErrorToReasons(erx);
    throw erx;
  }
};

/**
 *
 * @param request
 */
export const getTreatmentPlans = async (
  request: GetTreatmentPlansRequest,
): Promise<SuccessResponse> => {
  try {
    const treatmentPlans = [];
    const filterState = Boolean(request.filter) ? request.filter.state : undefined;
    const account = await UserService.findUserByAddress(request.senderAddress);
    const treatments = await TreatmentService.getTreamentplansByParticipantOrCareProvider(
      account._id,
    );
    const promises = treatments.map(async (treatmentData) => {
      const { contractAddress } = treatmentData;
      const { senderAddress } = request;
      const documentHash = await TreatmentContractService.generateDocumentHash(treatmentData);
      const treatmentContractReq: TreatmentPlanContractRequest = {
        contractAddress,
        documentHash,
        senderAddress,
      };

      const treatmentPlan = await TreatmentContractService.getTreatmentData(treatmentContractReq);
      if (filterState === undefined || treatmentPlan.state === filterState.toString()) {
        treatmentPlans.push({ treatment: treatmentData, contract: treatmentPlan });
      }
    });
    await Promise.all(promises);

    return {
      data: treatmentPlans,
      message: 'Behandelplannen met contract data succesvol opgehaald!',
    };

  } catch (erx) {
    blockchainErrorToReasons(erx);
    throw erx;
  }
};

/**
 * Pakt de drie meet waardes uit de blockchain:
 * - goalMeasurement
 * - endMeasurement
 * - startMeasurement
 * Afhankelijk van de status kunnen 1 of meerdere waardes
 * leeg zijn (nog niet toegevoegd door de careProvider van het contract)
 * @param request TreatmentIdRequest
 */
export const getTreatmentById = async (request: TreatmentByIdRequest):
  Promise<SuccesfullyReceivedTreatmentPlan> => {
  try {
    const { id, senderAddress } = request;

    const treatment = await getTreatmentplanById(id);
    const { contractAddress } = treatment;

    // genereer hash om contract op te halen.
    const documentHash = await TreatmentContractService.generateDocumentHash(treatment);
    const treatmentReq: TreatmentPlanContractRequest = {
      contractAddress, documentHash, senderAddress,
    };

    const treatmentPlan = await TreatmentContractService.getTreatmentData(treatmentReq);
    const goalMeasurement = await TreatmentContractService.getGoalMeasurement(treatmentReq);
    const startMeasurement = await TreatmentContractService.getStartMeasurement(treatmentReq);
    const endMeasurement = await TreatmentContractService.getEndMeasurement(treatmentReq);
    const pendingCoins = await getCoinReward(
      treatment, goalMeasurement, treatmentPlan.coinReward, endMeasurement,
    );
    const coinObject = treatmentPlanHasEndMeasurement(treatmentPlan)
      ? { finalCoins: pendingCoins } : { pendingCoins };

    return {
      data: {
        endMeasurement,
        goalMeasurement,
        startMeasurement,
        contractData: treatmentPlan,
        customMeasurements: treatment.customMeasurements,
        treatmentData: treatment,
        ...coinObject,
        ...{
          statusText: TreatmentContractService.getStatusTextDependingOnState(
            TreatmentPlanContractState[treatmentPlan.state],
          ),
        },
      },
      message: 'Behandelplan succesvol opgehaald',
    };
  } catch (erx) {
    blockchainErrorToReasons(erx);
    throw erx;
  }
};

const treatmentPlanIsDone = (treamentPlan: TreatmentPlanContract) => {
  return Boolean(treamentPlan)
    && treamentPlan.state === TreatmentPlanContractState[TreatmentPlanContractState.DONE];
};

const treatmentPlanHasEndMeasurement = (treamentPlan: TreatmentPlanContract) => {
  return Boolean(treamentPlan)
    && treamentPlan.state
    === TreatmentPlanContractState[TreatmentPlanContractState.END_MEASUREMENT_ADDED];
};
export const acceptTreatmentplan = async (request: TreatmentByIdRequest)
  : Promise<SuccessResponse> => {
  const { senderAddress } = request;
  const treatmentContract = await getTreatmentById(request);
  const documentHash = await TreatmentContractService.generateDocumentHash(treatmentContract.data.treatmentData);
  const { finalCoins } = treatmentContract.data;
  const { contractAddress } = treatmentContract.data.treatmentData;
  const { state, participant } = treatmentContract.data.contractData;
  let successMessage = '';
  try {

    if (TreatmentPlanContractState[state] === TreatmentPlanContractState.END_MEASUREMENT_ADDED) {
      await TreatmentContractService.signTreatmentToDoneAsParticipant(
        { contractAddress, senderAddress, documentHash }, finalCoins, participant);
      successMessage = 'Deelnemer heeft de eindmeting goedgekeurd!';
    } else {
      await TreatmentContractService.signTreatmentToStartAsParticipant(
        { contractAddress, senderAddress, documentHash });
      successMessage = 'Deelnemer heeft het behandelplan geaccepteerd!';
    }
    return {
      message: successMessage,
    };
  } catch (erx) {
    blockchainErrorToReasons(erx);
    throw erx;
  }
};

/**
 * Add a measurement.
 * Depending on contract state: Add a end / start measurement (as careprovider)
 * Or add custom measurement (as participant).
 * @param request
 */
export const addMeasurement = async (request: AddMeasurementRequest)
  : Promise<SuccessResponse> => {

  try {
    const treatmentContract = await getTreatmentById(request.treatmentData);
    const userInfo = await UserService.findUserByAddress(request.treatmentData.senderAddress);
    const { role } = userInfo;
    let message = '';
    // eigen measurement dus off-chain opslaan.
    if (TreatmentPlanContractState[treatmentContract.data.contractData.state]
      === TreatmentPlanContractState.STARTED
      && role === Role.Participant) {
      // voor nu is dit hardcoded, maar dit moet in de toekomst wel variabel worden.
      const customMeasurement: Measurement = { values: request.measurements };
      await TreatmentService.addCustomMeasurementToTreatment(
        request.treatmentData.id, customMeasurement,
      );
      message = 'Behandelplan tussentijdse meting succesvol toegevoegd!';
      return { message };
    }
    const addContractMeasurementResponse = await addContractMeasurement(
      request,
      treatmentContract.data.treatmentData,
    );
    return addContractMeasurementResponse;
  } catch (erx) {
    blockchainErrorToReasons(erx);
    throw erx;
  }
};

const addContractMeasurement = async (addMeasurementReq: AddMeasurementRequest,
  offchainTreatmentplan: TreatmentPlan) => {
  let message = '';
  const treatmentAfterMeasurement = await TreatmentContractService.addMeasurementToTreatment(
    addMeasurementReq, offchainTreatmentplan,
  );
  if (treatmentAfterMeasurement === TreatmentPlanContractState.SIGNED) {
    message = 'Behandelplan is nu officieel gestart!';
  } else {
    message = 'Behandelplan heeft nu een eindmeting en kan goedgekeurd worden door de deelnemer!';
  }
  return { message };
};

const getTreatmentplanById = async (id: string) => {
  const planFound = await TreatmentService.getTreatmentplanById(id);
  if (!planFound) {
    throw new Errors.NotFoundError(`Behandelplan met id '${id}' bestaat niet`);
  }
  return planFound;
};

/**
 * Receive active behandelplan based on getTreatments.
 * @param request
 */
export const getActiveTreatment = async (request: TreatmentActiveRequest) => {
  const participant = await UserService.findParticipantByAddress(request.senderAddress);
  const treatments = await TreatmentService.getTreamentplansByParticipantOrCareProvider(
    participant._id,
  );

  try {
    if (treatments.length > 0) {
      const { senderAddress, accessToken } = request;
      const mapContractData = await Promise.all(treatments.map(async (treatmentData) => {
        const documentHash = await TreatmentContractService.generateDocumentHash(treatmentData);
        const contractData = await TreatmentContractService.getTreatmentData({
          documentHash,
          senderAddress,
          contractAddress: treatmentData.contractAddress,
        });
        return { contractData, treatmentData };
      }));

      const undoneTreatments = mapContractData.find(
        treatment => TreatmentPlanContractState[treatment.contractData.state]
          !== TreatmentPlanContractState.DONE,
      );
      if (undoneTreatments !== undefined) {
        const getById: TreatmentByIdRequest = {
          accessToken,
          senderAddress,
          id: undoneTreatments.treatmentData._id,
        };
        return getTreatmentById(getById);
      }
    }
    throw new Errors.NotFoundError('Er is nog geen actief behandelplan');
  } catch (erx) {
    blockchainErrorToReasons(erx);
    throw erx;
  }
};

const calculateCoinReward = (
  measurementValue: TreatmentPlanContractMeasurement[],
  goalMeasurementValue: TreatmentPlanContractMeasurement[],
  coinReward: number,
) => {
  // calculate the correction you get for to high values in comparison to the measurement goals.
  const calcCorrection = measurementValue.map((val) => {
    const goalMeasurement = goalMeasurementValue.find(x => x.unit === val.unit);
    if (goalMeasurement !== undefined) {
      return coinReward - (goalMeasurement.quantity / val.quantity * coinReward);
    }
    return 0;
  });
  return coinReward - Math.floor(sumNumbers(calcCorrection));
};

const sumNumbers = (numbers: number[]) => {
  return numbers.reduce((a: number, b: number) => a + b, 0);
};

export const getCoinReward = (
  treatment: TreatmentPlan,
  goalMeasurement: TreatmentPlanContractMeasurement[],
  coinReward: any,
  endMeasurement?: TreatmentPlanContractMeasurement[],
) => {
  const lastDoneMeasurement = getLastMeasurement(endMeasurement, treatment);
  if (!lastDoneMeasurement) {
    return 0;
  }
  const reward = calculateCoinReward(lastDoneMeasurement, goalMeasurement, coinReward);
  return (reward === Infinity) ? 0 : (reward > coinReward) ? coinReward : reward;
};

const getLastMeasurement = (
  endMeasurement: TreatmentPlanContractMeasurement[],
  treatment: TreatmentPlan,
) => {
  if (endMeasurement && endMeasurement.length > 0) {
    return endMeasurement;
  }
  const { customMeasurements } = treatment;
  if (customMeasurements.length > 0) {
    const amountOfMeasurements = customMeasurements.length - 1;
    return customMeasurements[amountOfMeasurements].values;
  }
};
