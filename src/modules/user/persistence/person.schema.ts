import { Document, model, Model, Schema } from 'mongoose';
export const PERSON_MODEL_NAME = 'Person';

export const personProperties = {
  dateOfBirth: {
    required: true,
    type: Date,
  },
  firstName: {
    required: true,
    type: String,
  },
  gender: {
    required: true,
    type: String,
  },
  lastName: {
    required: true,
    type: String,
  },
  policyNumber: {
    required: true,
    type: String,
  },
};
const personSchema = new Schema(personProperties);

export interface Person {
  dateOfBirth: Date;
  firstName: string;
  gender: string;
  lastName: string;
  policyNumber: string;
}

type ModelType = Person & Document;
let person: Model<ModelType>;
try {
  person = model<ModelType>(PERSON_MODEL_NAME);
} catch (err) {
  person = model<ModelType>(PERSON_MODEL_NAME, personSchema);
}
export default person;
