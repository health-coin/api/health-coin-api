import 'mocha';
import { server } from '../../../src/index';
import * as MainController from '../../../src/main.controller';
import { TransactionRequest } from '../../../src/modules/transaction/typings';
import InvalidPropertyTester from '../utils/InvalidPropertyTester';
import MissingPropertyTester from '../utils/MissingPropertyTester';
import invalidInput from '../validation/invalid/invalid-input';
import validInput from '../validation/valid/valid-input';

const VALID_REQUEST: TransactionRequest = {
  amount: validInput.AMOUNT_ONE,
  from: validInput.ADDRESS,
  to: validInput.ADDRESS,
};

const AMOUNT_PROPERTY = 'amount';
const TO_PROPERTY = 'to';
const FROM_PROPERTY = 'from';
const invalidTester = new InvalidPropertyTester(VALID_REQUEST, MainController.createTransaction);
const missingTester = new MissingPropertyTester(VALID_REQUEST, MainController.createTransaction);

describe('Make transfer test', () => {
  after('Close server', () => {
    server.close();
  });

  describe(`Without ${TO_PROPERTY}`, () => {
    missingTester.testProperty(TO_PROPERTY);
  });

  describe(`With invalid ${TO_PROPERTY}`, () => {
    invalidTester.testProperty(TO_PROPERTY, invalidInput.ADDRESS_CONTAINS_SYMBOL);
  });

  describe(`Without ${FROM_PROPERTY}`, () => {
    missingTester.testProperty(FROM_PROPERTY);
  });

  describe(`With invalid ${FROM_PROPERTY}`, () => {
    invalidTester.testProperty(FROM_PROPERTY, invalidInput.ADDRESS_CONTAINS_SYMBOL);
  });

  describe(`Without ${AMOUNT_PROPERTY}`, () => {
    missingTester.testProperty(AMOUNT_PROPERTY);
  });

  describe(`With negative ${AMOUNT_PROPERTY}`, () => {
    invalidTester.testProperty(AMOUNT_PROPERTY, invalidInput.NEGATIVE_AMOUNT);
  });

  describe(`With decimal ${AMOUNT_PROPERTY}`, () => {
    invalidTester.testProperty(AMOUNT_PROPERTY, invalidInput.DECIMAL_AMOUNT);
  });
});
