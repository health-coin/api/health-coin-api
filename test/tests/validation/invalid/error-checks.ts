import * as Chai from 'chai';

const should = Chai.should();

export interface Error {
  message: string;
  code: string;
}

const BAD_REQUEST = 'BadRequest';
const NOT_FOUND = 'NotFound';
const CONFLICT = 'Conflict';
const UNAUTHORIZED = 'Unauthorized';
const FORBIDDEN = 'Forbidden';

export const validateError = (error: Error) => {
  should.exist(error);
  should.exist(error.message);
  should.exist(error.code);
};

export const validateBadRequest = (error: Error) => {
  validateError(error);
  error.code.should.equal(BAD_REQUEST);
};

export const validateNotFound = (error: Error) => {
  validateError(error);
  error.code.should.equal(NOT_FOUND);
};

export const validateConflict = (error: Error) => {
  validateError(error);
  error.code.should.equal(CONFLICT);
};

export const validateUnAuthorized = (error: Error) => {
  validateError(error);
  error.code.should.equal(UNAUTHORIZED);
};

export const validateForbidden = (error: Error) => {
  validateError(error);
  error.code.should.equal(FORBIDDEN);
};
