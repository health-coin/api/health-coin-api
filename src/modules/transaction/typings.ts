export interface SuccessResponse {
  message: string;
  data: {};
}

export interface BalanceQuery {
  id: string;
}

export interface TransactionRequest {
  from: string;
  to: string;
  amount: number;
}

export interface RewardTransactionQuery {
  to: string;
  amount: number;
}

export interface BalanceReponse extends SuccessResponse {
  data: { balance: number };
}
