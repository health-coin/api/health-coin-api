import * as Moment from 'moment';
import { TreatmentListFilterOptions, TreatmentPlanContractState } from './typings';
const ID_REGEX = /^[a-fA-F0-9]{24}$/;
const NUMBER_REGEX = /^\d+$/;
const NAME_REGEX = /^[a-zA-Z ,.'-]+$/;

export const invalidId = (id: string) => {
  return !ID_REGEX.test(id);
};

export const invalidPlanId = (num: any) => {
  return !NUMBER_REGEX.test(num);
};

export const invalidStartdate = (dateString: string) => {
  if (invalidDate(dateString)) {
    return true;
  }
  const date = new Date(dateString);
  return date >= new Date();
};

export const invalidStatus = (status: string) => {
  return !NAME_REGEX.test(status);
};

export const invalidUnit = (unit: string) => {
  return !NAME_REGEX.test(unit);
};

export const invalidEnddate = (dateString: string) => {
  if (invalidDate(dateString)) {
    return true;
  }
  const date = new Date(dateString);
  return new Date() >= date;
};

export const invalidObject = (object: any) => {
  return !(object instanceof Object);
};

export const invalidArray = (arr: any[]) => {
  return !(arr instanceof Array);
};

export const invalidReward = (num: any) => {
  return !NUMBER_REGEX.test(num);
};

export const invalidQuantity = (num: any) => {
  return !NUMBER_REGEX.test(num) || num <= 0;
};

export const invalidDate = (dateString: string) => {
  const date = Moment(dateString, ['YYYY-MM-DD']);
  return !date.isValid();
};

export const invalidFilterProperty = (filter: TreatmentListFilterOptions) => {
  const { state } = filter;
  return state === undefined || TreatmentPlanContractState[state] === undefined;
};
