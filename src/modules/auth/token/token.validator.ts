import * as Errors from 'restify-errors';
import * as ValidatorFuncs from '../validator-funcs';
import {
  CreateTokenRequest, DecodeTokenRequest, RefreshTokenRequest,
  RevokeTokenRequest, TokenByIdRequest,
} from './typings';

export const validateFindTokenRequest = (request: TokenByIdRequest) => {
  const ID_PARAM = 'id parameter';
  checkRequired(request.id, ID_PARAM);
  checkValid(id => ValidatorFuncs.invalidId(id), request.id, ID_PARAM);
};

export const validateCreateTokenRequest = (request: CreateTokenRequest) => {
  const ADDRESS_PROPERTY = 'address';
  const ROLE_PROPERTY = 'role';
  const address = request[ADDRESS_PROPERTY];
  const role = request[ROLE_PROPERTY];

  checkRequired(address, ADDRESS_PROPERTY);
  checkRequired(role, ROLE_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidAddress(x), address, ADDRESS_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidRole(x), role, ROLE_PROPERTY);
};

export const validateRevokeTokenRequest = (request: RevokeTokenRequest) => {
  const ACCESS_TOKEN_PROPERTY = 'accessToken';
  checkRequired(request[ACCESS_TOKEN_PROPERTY], ACCESS_TOKEN_PROPERTY);
};

export const validateRefreshTokenRequest = (request: RefreshTokenRequest) => {
  const REFRESH_TOKEN_PROPERTY = 'refreshToken';
  checkRequired(request[REFRESH_TOKEN_PROPERTY], REFRESH_TOKEN_PROPERTY);
};

export const validateDecodeRequest = (request: DecodeTokenRequest) => {
  const ACCESS_TOKEN_PROPERTY = 'accessToken';
  checkRequired(request[ACCESS_TOKEN_PROPERTY], ACCESS_TOKEN_PROPERTY);
};

const checkRequired = (property: any, propertyName: string) => {
  if (!property) {
    throw new Errors.BadRequestError(`${propertyName} is verplicht`);
  }
};

const checkValid = (
  isInvalidFunction: (property: any) => boolean, property: any, propertyName: string,
) => {
  if (isInvalidFunction(property)) {
    throw new Errors.BadRequestError(`${propertyName} is ongeldig`);
  }
};
