import { Document, model, Model, Schema } from 'mongoose';
export const ACCOUNT_MODEL_NAME = 'Account';
import Role from '../Role';

const accountOptions = {
  collection: 'users',
  discriminatorKey: 'role',
};

const accountSchema = new Schema({
  address: {
    required: true,
    type: String,
  },
  email: {
    required: true,
    type: String,
  },
  password: {
    required: true,
    type: String,
  },
  registeredAt: {
    default: Date.now(),
    required: false,
    type: Date,
  },
}, accountOptions);

export interface Account {
  _id: string;
  role: Role;
  address: string;
  email: string;
  password: string;
  registeredAt?: Date;
}

type ModelType = Account & Document;
let account: Model<ModelType>;
try {
  account = model<ModelType>(ACCOUNT_MODEL_NAME);
} catch (err) {
  account = model<ModelType>(ACCOUNT_MODEL_NAME, accountSchema);
}
export default account;
