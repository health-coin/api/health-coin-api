import { TreatmentPlan } from './persistence/treatment-plan.schema';

export interface SuccessResponse {
  message: string;
  data?: any;
}

export interface CreateTreatmentplanRequest {
  coinReward: number;
  endDate: string;
  participantId: string;
  careProviderAddress?: string; // extracted from token
  careProviderId?: string; // gets extracted by address
  accessToken: string;
  measurements: TreatmentPlanContractMeasurement[];
}

export interface TreatmentByIdRequest {
  accessToken: string;
  id: string;
  senderAddress?: string; // extracted from token.
}

export interface TreatmentListFilterOptions {
  state: TreatmentPlanContractState;
}

export interface GetTreatmentPlansRequest {
  senderAddress?: string;
  accessToken: string;
  filter?: TreatmentListFilterOptions;
}

export interface TreatmentActiveRequest {
  senderAddress?: string;
  accessToken: string;
}

export interface AddMeasurementRequest {
  treatmentData: TreatmentByIdRequest;
  measurements: TreatmentPlanContractMeasurement[];
}

export interface SuccesfullyAddedTreatmentplan extends SuccessResponse {
  data: TreatmentPlan;
}
/**
 * This is for a treatment plan start measurement / end measurement en goal measurement added.
 */
export interface CompleteTreatmentPlan {
  startMeasurement: object;
  goalMeasurement: object;
  contractData: TreatmentPlanContract;
  endMeasurement: object;
  customMeasurements: any[];
  treatmentData: TreatmentPlan;
  finalCoins?: number;
}

export interface SuccesfullyReceivedTreatmentPlan extends SuccessResponse {
  data: CompleteTreatmentPlan;
}

/**
 * Contract as received from the blockchain
 */
export interface TreatmentPlanContract {
  careTaker: string;
  participant: string;
  coinReward: any;
  plannedEndDate: any;
  startDate: any;
  state: string;
  completionDate: any;
  redeemed: any;
}

/**
 * Combination of parameters which are often required for requesting a treatmentcontract
 */
export interface TreatmentPlanContractRequest {
  contractAddress: string;
  documentHash: string;
  senderAddress: string;
}

/**
 * For now we got measurements hardcoded into the contract.
 */
export interface TreatmentPlanContractMeasurement {
  unit: string;
  quantity: number;
}

/**
 * Contract states used on Blockchain.
 * - DONE: Fully done (added endMeasurement by careProvider);
 * - STARTED: Signed by participant & careProvider
 * - PENDING:
 */
export enum TreatmentPlanContractState {
  DONE = 0,
  STARTED = 1,
  PENDING = 2,
  SIGNED = 3,
  END_MEASUREMENT_ADDED = 4,
}

export interface CreateTreatmentContractRequest {
  careProviderAddress: string;
  participantAddress: string;
  plannedEndDate: any;
  documentHash: string;
  coinReward: number;
  measurements: TreatmentPlanContractMeasurement[];
}
