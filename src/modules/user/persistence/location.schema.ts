import { Document, model, Model, Schema } from 'mongoose';
import { Address, ADDRESS_MODEL_NAME } from './address.schema';

const { ObjectId } = Schema.Types;
export const LOCATION_MODEL_NAME = 'Location';

const locationSchema = new Schema({
  address: {
    ref: ADDRESS_MODEL_NAME,
    required: true,
    type: ObjectId,
  },
  name: {
    required: true,
    type: String,
  },
});

export interface Location {
  address: Address;
  name: string;
}

type ModelType = Location & Document;
let location: Model<ModelType>;
try {
  location = model<ModelType>(LOCATION_MODEL_NAME);
} catch (err) {
  location = model<ModelType>(LOCATION_MODEL_NAME, locationSchema);
}
export default location;
