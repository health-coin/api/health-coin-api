import * as Errors from 'restify-errors';
import {
  AddMeasurementRequest,
  CreateTreatmentplanRequest,
  GetTreatmentPlansRequest,
  TreatmentByIdRequest,
  TreatmentPlanContractMeasurement,
} from './typings';
import * as ValidatorFuncs from './validator-funcs';

export const validateCreateTreatmentRequest = (request: CreateTreatmentplanRequest) => {
  const COIN_REWARD_PROPERTY = 'coinReward';
  const END_DATE_PROPERTY = 'endDate';
  const MEASUREMENTS_PROPERTY = 'measurements';
  const PARTICIPANT_ID_PROPERTY = 'participantId';

  checkRequired(request.coinReward, COIN_REWARD_PROPERTY);
  checkRequired(request.endDate, END_DATE_PROPERTY);
  checkRequired(request.participantId, PARTICIPANT_ID_PROPERTY);
  checkRequired(request.measurements, MEASUREMENTS_PROPERTY);

  checkValid(x => ValidatorFuncs.invalidReward(x), request.coinReward, COIN_REWARD_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidEnddate(x), request.endDate, END_DATE_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidId(x), request.participantId, PARTICIPANT_ID_PROPERTY);

  checkValid(x => ValidatorFuncs.invalidArray(x), request.measurements, MEASUREMENTS_PROPERTY);
  validateValueProperties(request.measurements);
};

export const validateGetTreatmentPlansRequest = (request: GetTreatmentPlansRequest) => {
  const FILTER_PROPERTY = 'filter';
  if (request.filter) {
    checkValid(x => ValidatorFuncs.invalidObject(x), request.filter, FILTER_PROPERTY);
    checkValid(x => ValidatorFuncs.invalidFilterProperty(x), request.filter, FILTER_PROPERTY);
  }
};

export const validateAddMeasurement = (request: AddMeasurementRequest) => {
  const MEASUREMENT_PROPERTY = 'measurements';
  checkRequired(request.measurements, MEASUREMENT_PROPERTY);
  validateValueProperties(request.measurements);
};

export const validateTreatmentPlanByIdRequest = (request: TreatmentByIdRequest) => {
  const ID_PROPERTY = 'id';
  checkRequired(request.id, ID_PROPERTY);
  checkValid(x => ValidatorFuncs.invalidId(x), request.id, ID_PROPERTY);
};

const validateValueProperties = (request: TreatmentPlanContractMeasurement[]) => {
  request.forEach((obj) => {
    const QUANITY_PROPERTY = 'quantity';
    const UNIT_PROPERTY = 'unit';
    const { quantity, unit } = obj;

    checkRequired(unit, UNIT_PROPERTY);
    checkRequired(quantity, QUANITY_PROPERTY);
    checkValid(x => ValidatorFuncs.invalidUnit(x), unit, UNIT_PROPERTY);
    checkValid(x => ValidatorFuncs.invalidQuantity(x), quantity, QUANITY_PROPERTY);
  });
};

const checkRequired = (property: any, propertyName: string) => {
  if (!property) {
    throw new Errors.BadRequestError(`${propertyName} is verplicht`);
  }
};

const checkValid = (
  isInvalidFunction: (property: any) => boolean, property: any, propertyName: string,
) => {
  if (isInvalidFunction(property)) {
    throw new Errors.BadRequestError(`${propertyName} is ongeldig`);
  }
};
