import { Document, model, Model, Schema } from 'mongoose';

export const TOKEN_MODEL_NAME = 'Token';

const tokenSchema = new Schema({
  accessToken: {
    required: true,
    type: String,
  },
  createdAt: {
    default: Date.now(),
    type: Date,
  },
  refreshToken: {
    required: true,
    type: String,
  },
});

export interface Token {
  _id: string;
  accessToken: string;
  createdAt: Date;
  refreshToken: string;
  expirationDate?: Date;
}

type ModelType = Token & Document;
let token: Model<ModelType>;
try {
  token = model<ModelType>(TOKEN_MODEL_NAME);
} catch (err) {
  token = model<ModelType>(TOKEN_MODEL_NAME, tokenSchema);
}
export default token;
