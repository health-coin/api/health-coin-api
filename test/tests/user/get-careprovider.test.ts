import * as Sinon from 'sinon';
import { server } from '../../../src/index';
import { CareProvider } from '../../../src/modules/user/persistence/care-provider.schema';
import { GetCareProviderRequest } from '../../../src/modules/user/typings';
import * as UserController from '../../../src/modules/user/user.controller';
import * as UserRoute from '../../../src/modules/user/user.route';
import * as PersistenceUtils from '../persistence-utils';
import * as TestUtils from '../test-utils';
import InvalidPropertyTester from '../utils/InvalidPropertyTester';
import MissingPropertyTester from '../utils/MissingPropertyTester';
import * as ErrorChecks from '../validation/invalid/error-checks';
import * as ValidBodyChecks from '../validation/valid/body-checks';
import validInput from '../validation/valid/valid-input';

const VALID_REQUEST: GetCareProviderRequest = TestUtils.createClone({
  userAddress: validInput.BLOCKCHAIN_ADDRESS_TWO,
});

const NON_EXISTING_ADDRESS = '0xE6b156f456253C9Ad76d62212Fc4317fb784FbC1';
const USER_ADDRESS_PROPERTY = 'userAddress';

const missingTester = new MissingPropertyTester(VALID_REQUEST, UserRoute.getCareProvider);
const notFoundTester = new InvalidPropertyTester(
  VALID_REQUEST,
  UserController.getCareProvider,
  TestUtils.NOT_FOUND_MESSAGE,
  ErrorChecks.validateNotFound,
);

describe('Get careprovider info', () => {
  let careProvider: CareProvider;

  before('Create test objects', async () => {
    Sinon.stub(UserController, 'createEthereumAccount').callsFake(async (password: string) => {
      return VALID_REQUEST.userAddress;
    });
    careProvider = await PersistenceUtils.createOrFindCareProvider();
    VALID_REQUEST[USER_ADDRESS_PROPERTY] = careProvider.address;
  });

  after('Restore stubs', async () => {
    Sinon.restore();
  });

  after('Close server', () => {
    server.close();
  });

  describe(`Without ${USER_ADDRESS_PROPERTY}`, () => {
    missingTester.testProperty(USER_ADDRESS_PROPERTY);
  });

  describe('With the address of a non existing careprovider', async () => {
    notFoundTester.testProperty(USER_ADDRESS_PROPERTY, NON_EXISTING_ADDRESS);
  });

  describe('With the addrress of the careprovider', async () => {
    it('Should return info of the careprovider', async () => {
      const result = await UserController.getCareProvider(VALID_REQUEST);
      ValidBodyChecks.validateSuccessResponse(result);
      const returnedCareProvider = result.data;
      ValidBodyChecks.validateCareProvider(returnedCareProvider);
      returnedCareProvider.speciality.should.equal(validInput.SPECIALITY);
    });
  });
});
