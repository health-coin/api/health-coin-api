import * as TreatmentController from './treatment.controller';
import * as TreatmentValidator from './treatment.validator';
import {
  AddMeasurementRequest,
  CreateTreatmentplanRequest,
  GetTreatmentPlansRequest,
  SuccessResponse,
  TreatmentActiveRequest,
  TreatmentByIdRequest,
} from './typings';

export const createTreatmentplan = async (request: CreateTreatmentplanRequest,
): Promise<SuccessResponse> => {
  TreatmentValidator.validateCreateTreatmentRequest(request);
  return TreatmentController.createTreatmentPlan(request);
};

export const getTreatmentPlans = async (request: GetTreatmentPlansRequest) => {
  TreatmentValidator.validateGetTreatmentPlansRequest(request);
  return TreatmentController.getTreatmentPlans(request);
};

export const getTreatmentplanById = async (request: TreatmentByIdRequest) => {
  TreatmentValidator.validateTreatmentPlanByIdRequest(request);
  return TreatmentController.getTreatmentById(request);
};

export const getActiveTreatmentplan = async (request: TreatmentActiveRequest) => {
  return TreatmentController.getActiveTreatment(request);
};

export const acceptTreatmentplan = async (request: TreatmentByIdRequest) => {
  TreatmentValidator.validateTreatmentPlanByIdRequest(request);
  return TreatmentController.acceptTreatmentplan(request);
};

export const addMeasurement = async (request: AddMeasurementRequest) => {
  TreatmentValidator.validateAddMeasurement(request);
  return TreatmentController.addMeasurement(request);
};
