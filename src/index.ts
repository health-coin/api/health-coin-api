import * as W3 from 'web3';
import mainConfig from '../config/main.config';
import mongoConfig from '../config/mongo.config';
const Web3 = require('web3'); // tslint:disable-line
import * as Restify from 'restify';
import specification from '../docs/specification';
import * as MainBridge from './main-rest.bridge';
import { MongoDB } from './mongoDB';
import * as RequestCleaner from './request-cleaner';

export const web3: W3.default = new Web3(
  new Web3.providers.HttpProvider(
    `${mainConfig.ethereumHost}:${mainConfig.ethereumPort}`),
  undefined,
  { transactionConfirmationBlocks: 1 },
);

export const server = Restify.createServer();

server.use(Restify.plugins.requestLogger());
server.use(Restify.plugins.acceptParser(server.acceptable));
server.use(Restify.plugins.queryParser({
  mapParams: true,
}));
server.use(Restify.plugins.bodyParser({
  multiples: true,
}));
server.use((req, res, next) => {
  RequestCleaner.cleanRequest(req.body);
  RequestCleaner.cleanRequest(req.params);
  return next();
});

MongoDB.getInstance(mongoConfig, (input: any) => { process.stdout.write(`${input} \n`); });

server.get('/', (req, res, next) => {
  res.send(`Welcome to Health coins API, running Web3 version: ${web3.version}`);
});

MainBridge.loadServices(server);

server.get('/docs/*', Restify.plugins.serveStatic(
  { directory: './public', default: 'index.html' },
));
server.get('/docs/swagger.json', (req, res) => res.send(specification));

server.listen(mainConfig.port, () => {
  process.stdout.write(`${server.name} listening at ${server.url}\n`);
});
