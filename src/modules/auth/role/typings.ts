import { Route } from './persistence/role.schema';

export interface CreateRoleRequest {
  name: string;
  allowedRoutes: Route[];
}

export interface RoleByNameRequest {
  name: string;
}

export interface DeleteRoleRequest {
  name: string;
}

export interface HasRightsRequest {
  role: string;
  route: Route;
}
