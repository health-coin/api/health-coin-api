import 'mocha';
import * as Sinon from 'sinon';
import { server } from '../../../src/index';
import { LoginRequest } from '../../../src/main-typings';
import * as MainController from '../../../src/main.controller';
import * as UserController from '../../../src/modules/user/user.controller';
import * as PersistenceUtils from '../persistence-utils';
import * as TestUtils from '../test-utils';
import InvalidPropertyTester from '../utils/InvalidPropertyTester';
import * as ErrorChecks from '../validation/invalid/error-checks';
import * as ValidBodyChecks from '../validation/valid/body-checks';
import validInput from '../validation/valid/valid-input';

const VALID_REQUEST: LoginRequest = {
  email: validInput.EMAIL_TJ,
  password: validInput.PASSWORD_TEST,
};

const EMAIL_PROPERTY = 'email';
const PASSWORD_PROPERTY = 'password';

const unauthorizedTester = new InvalidPropertyTester(
  VALID_REQUEST,
  MainController.loginUser,
  TestUtils.UNAUTHORIZED_MESSAGE,
  ErrorChecks.validateUnAuthorized,
);

describe('login user integration test', () => {
  before('Create a test account', async () => {
    Sinon.stub(UserController, 'createEthereumAccount').callsFake(async (password: string) => {
      return validInput.BLOCKCHAIN_ADDRESS_ONE;
    });

    const clone = TestUtils.createClone(validInput.PARTICIPANT_REQUEST);
    clone.email = VALID_REQUEST[EMAIL_PROPERTY];
    clone.password = VALID_REQUEST[PASSWORD_PROPERTY];
    await PersistenceUtils.createParticipant(clone);
  });

  after('Delete the test account', async () => {
    Sinon.restore();
    await PersistenceUtils.deleteParticipant(VALID_REQUEST[EMAIL_PROPERTY]);
    server.close();
  });

  describe(`With valid ${EMAIL_PROPERTY} but wrong ${PASSWORD_PROPERTY}`, () => {
    unauthorizedTester.testProperty(PASSWORD_PROPERTY, validInput.PASSWORD_TEST2);
  });

  describe(`With valid ${PASSWORD_PROPERTY} but wrong ${EMAIL_PROPERTY}`, () => {
    unauthorizedTester.testProperty(EMAIL_PROPERTY, validInput.EMAIL);
  });

  describe('With a valid request', () => {
    it('Should return the user info', async () => {
      const result = await MainController.loginUser(TestUtils.createClone(VALID_REQUEST));
      ValidBodyChecks.validateSuccessResponse(result);
      ValidBodyChecks.validateAccount(result.data);
      ValidBodyChecks.validateToken(result.data.token, true);
    });
  });
});
