export interface MongoConfig {
  mongoAddress: string;
  mongoDatabase: string;
  mongoUser: string;
  mongoPassword: string;
}

const mongoConfig: MongoConfig = {
  mongoAddress: process.env.MONGO_ADDRESS || '127.0.0.1',
  mongoDatabase: process.env.MONGO_DATABASE || 'healthCoinDB',
  mongoPassword: process.env.MONGO_PASSWD || '',
  mongoUser: process.env.MONGO_USER || '',
};

export default mongoConfig;
