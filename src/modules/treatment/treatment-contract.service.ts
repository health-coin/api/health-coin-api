import * as FS from 'fs';
import { web3 } from '../..';
import { rewardHealthcoins } from '../../main.controller';
import { RewardTransactionQuery } from '../transaction/typings';
import { TreatmentPlan } from './persistence/treatment-plan.schema';
import {
  AddMeasurementRequest,
  CreateTreatmentContractRequest,
  TreatmentPlanContract,
  TreatmentPlanContractMeasurement,
  TreatmentPlanContractRequest,
  TreatmentPlanContractState,
} from './typings';

const treatmentABI = JSON.parse(
  FS.readFileSync('./build/contracts/TreatmentPlan.json').toString(),
);

const getTreatmentContract = (address?) => {
  return new web3.eth.Contract(treatmentABI.abi, address);
};

export const createTreatmentContract = async (request: CreateTreatmentContractRequest) => {
  const {
    careProviderAddress, participantAddress, coinReward,
    plannedEndDate, documentHash, measurements,
  } = request;

  const deployArguments = [
    documentHash,
    participantAddress,
    web3.eth.abi.encodeParameter('uint256', plannedEndDate),
    web3.eth.abi.encodeParameter('uint256', coinReward),
    measurements,
  ];

  const byteCode = treatmentABI.bytecode;
  return new Promise((resolve, reject) => {
    getTreatmentContract().deploy({
      arguments: deployArguments,
      data: byteCode,
    }).estimateGas({}, (error, gasAmount) => {
      const required = gasAmount * 3;
      getTreatmentContract().deploy({
        arguments: deployArguments,
        data: byteCode,
      })
        .send({ from: careProviderAddress, gas: required })
        .on('receipt', (message) => {
          const contractAddress = message.contractAddress;
          resolve(contractAddress);
        })
        .on('confirmation', message => console.log('confirmation: ', message))
        .on('error', (message) => {
          reject(message);
        });
    });
  });
};

export const signTreatmentToStartAsParticipant = async (
  req: TreatmentPlanContractRequest,
) => {
  const contract = getTreatmentContract(req.contractAddress);
  return new Promise((resolve, reject) => {
    contract.methods.signTreatmentToStartAsParticipant(req.documentHash)
      .send({ from: req.senderAddress })
      .on('receipt', res => resolve(res))
      .on('error', erx => reject(erx));
  });
};

export const signTreatmentToDoneAsParticipant = async (
  req: TreatmentPlanContractRequest,
  coinReward: number,
  participantAddress: string,
) => {
  const contract = getTreatmentContract(req.contractAddress);
  return new Promise((resolve, reject) => {
    contract.methods.signTreatmentToBeDoneAsParticipant(req.documentHash)
      .send({ from: req.senderAddress })
      .on('receipt', async (res) => {
        await transferHealthCoins({
          to: participantAddress,
          amount: coinReward,
        });
        resolve(res);
      })
      .on('error', erx => reject(erx));
  });
};

const transferHealthCoins = async (coinRequest: RewardTransactionQuery) => {
  return await rewardHealthcoins(coinRequest);

};

export const addMeasurementToTreatment = async (
  req: AddMeasurementRequest, offChainTreatment: TreatmentPlan,
): Promise<TreatmentPlanContractState> => {
  const gasPricePerAction = 250000;
  const treatmentData = req.treatmentData;
  const measurementData = req.measurements;
  const documentHash = await generateDocumentHash(offChainTreatment);
  const treatmentContractReq: TreatmentPlanContractRequest = {
    documentHash,
    contractAddress: offChainTreatment.contractAddress,
    senderAddress: treatmentData.senderAddress,
  };

  const contract = getTreatmentContract(offChainTreatment.contractAddress);
  const treatmentContract = await getTreatmentData(treatmentContractReq);
  const { state } = treatmentContract;
  const storageManipulationGas = (measurementData.length * 2) * gasPricePerAction;
  try {
    // security logic gets handled by contract
    if (treatmentPlanIsSigned(treatmentContract)) {
      await contract.methods.addFirstMeasurement(
        documentHash, measurementData,
      ).send({
        from: treatmentData.senderAddress,
        gas: storageManipulationGas,
      });
    } else {
      // update endmeasurement on blockchain side.
      await contract.methods.addEndMeasurement(
        documentHash, measurementData,
      ).send({
        from: treatmentData.senderAddress,
        gas: storageManipulationGas,
      });
    }
  } catch (erx) {
    throw erx;
  }
  return TreatmentPlanContractState[state];
};

const treatmentPlanIsSigned = (treatmentPlan: TreatmentPlanContract) => {
  const state = TreatmentPlanContractState[treatmentPlan.state];
  return state === TreatmentPlanContractState.SIGNED;
};

export const getTreatmentData = async (
  req: TreatmentPlanContractRequest,
): Promise<TreatmentPlanContract> => {
  const contract = getTreatmentContract(req.contractAddress);
  const treatment = await contract.methods
    .getTreatment(req.documentHash)
    .call({ from: req.senderAddress });
  const treatmentDateInMilliSeconds = treatment.startDate.toNumber() * 1000;

  return {
    careTaker: treatment.careTaker,
    coinReward: treatment.coinReward.toNumber(),
    completionDate: treatment.completionDate.toNumber(),
    participant: treatment.participant,
    plannedEndDate: treatment.plannedEndDate.toNumber(),
    redeemed: treatment.redeemed,
    startDate: treatmentDateInMilliSeconds,
    state: TreatmentPlanContractState[treatment.state],
  };
};

export const getGoalMeasurement = async (
  req: TreatmentPlanContractRequest,
): Promise<TreatmentPlanContractMeasurement[]> => {
  const contract = getTreatmentContract(req.contractAddress);
  const goalMeasurement = await contract.methods.getGoalMeasurement(req.documentHash).call(
    { from: req.senderAddress },
  );
  const goalMeasurementObj: TreatmentPlanContractMeasurement[] = goalMeasurement;
  return structArrayToTypedArray(goalMeasurementObj);
};

export const getStartMeasurement = async (
  req: TreatmentPlanContractRequest,
): Promise<TreatmentPlanContractMeasurement[]> => {
  const contract = getTreatmentContract(req.contractAddress);
  const startMeasurement = await contract.methods.getStartMeasurement(req.documentHash).call(
    { from: req.senderAddress },
  );
  const startMeasurementObj: TreatmentPlanContractMeasurement[] = startMeasurement;
  return structArrayToTypedArray(startMeasurementObj);
};

export const getEndMeasurement = async (
  req: TreatmentPlanContractRequest,
): Promise<TreatmentPlanContractMeasurement[]> => {
  const contract = getTreatmentContract(req.contractAddress);
  const endMeasurement = await contract.methods.getEndMeasurement(req.documentHash).call(
    { from: req.senderAddress },
  );
  const endMeasurementObj: TreatmentPlanContractMeasurement[] = endMeasurement;
  return structArrayToTypedArray(endMeasurementObj);
};

const structArrayToTypedArray = (array: any[]): TreatmentPlanContractMeasurement[] => {
  const formattedArray: TreatmentPlanContractMeasurement[] = [];
  array.forEach((element, index: number) => {
    formattedArray.push({
      quantity: array[index][1].toNumber(),
      unit: array[index][0],
    });
  });
  return formattedArray;
};

/**
 * We generate a document hash of the
 * - participant
 * - careProvider
 * to validate its not tempered
 * @param treatment Treatment from the database
 */
export const generateDocumentHash = async (treatment: TreatmentPlan): Promise<string> => {
  const { participantId, careProviderId } = treatment;
  const hash = web3.utils.sha3(participantId + careProviderId);
  return hash;
};

export const getStatusTextDependingOnState = (state: TreatmentPlanContractState) => {
  switch (state) {
    case 0:
      return 'Behandel plan heeft een eindmeting en wacht op goedkeuring van de deelnemer';
    case 1:
      return 'Behandel plan is gestart';
    case 2:
      return 'Behandel plan is in afwachting van goedkeuring door deelnemer';
    case 3:
      return 'Behandel plan is goedgekeurd door deelnemer en start als de eerste meting is toegevoegd';
    case 4:
      return 'Behandelplan is goedgekeurd door deelnemer en is nu afgerond';
  }
};
