import swaggerJsdoc = require('swagger-jsdoc');
import mainConfig from '../config/main.config';

const OPEN_API_VERSION = '3.0.0';
const API_DOCS_PATH = './docs/files/*.docs.yml';

const options = {
  apis: [API_DOCS_PATH],
  swaggerDefinition: {
    info: {
      title: mainConfig.serverName,
      version: mainConfig.serverVersion,
    },
    openapi: OPEN_API_VERSION,
  },
};

const specification = swaggerJsdoc(options);
export default specification;
