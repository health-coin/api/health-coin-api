import 'mocha';
import * as Sinon from 'sinon';
import { server } from '../../../src/index';
import * as TreatmentRoute from '../../../src/modules/treatment/treatment.route';
import { CreateTreatmentplanRequest } from '../../../src/modules/treatment/typings';
import { Participant } from '../../../src/modules/user/persistence/participant.schema';
import * as UserController from '../../../src/modules/user/user.controller';
import * as PersistenceUtils from '../persistence-utils';
import * as TestUtils from '../test-utils';
import InvalidPropertyTester from '../utils/InvalidPropertyTester';
import MissingPropertyTester from '../utils/MissingPropertyTester';

import invalidInput from '../validation/invalid/invalid-input';
import validInput from '../validation/valid/valid-input';

const VALID_REQUEST: CreateTreatmentplanRequest = TestUtils.createClone(
  validInput.TREATMENT_REQUEST,
);

const COIN_REWARD_PROPERTY = 'coinReward';
const END_DATE_PROPERTY = 'endDate';
const MEASUREMENTS_PROPERTY = 'measurements';
const PARTICIPANT_ID_PROPERTY = 'participantId';

const QUANITY_PROPERTY = 'quantity';
const UNIT_PROPERTY = 'unit';

const invalidTester = new InvalidPropertyTester(VALID_REQUEST, TreatmentRoute.createTreatmentplan);
const missingTester = new MissingPropertyTester(VALID_REQUEST, TreatmentRoute.createTreatmentplan);

const deleteMeasurementPropertyFunc = (validObject: any, propertyName: string): void => {
  delete validObject[MEASUREMENTS_PROPERTY][0][propertyName];
};

const setMeasurementPropertyFunc = (
  validObject: any, propertyName: string, property: any,
): void => {
  validObject[MEASUREMENTS_PROPERTY][0][propertyName] = property;
};

describe('Create treatmentplan test', () => {
  let participant: Participant;

  before('Create test objects', async () => {
    Sinon.stub(UserController, 'createEthereumAccount').callsFake(async (password: string) => {
      return validInput.BLOCKCHAIN_ADDRESS_ONE;
    });
    participant = await PersistenceUtils.createParticipant();
    VALID_REQUEST[PARTICIPANT_ID_PROPERTY] = participant._id;
  });

  after('Close the server, delete test objects', async () => {
    Sinon.restore();
    await PersistenceUtils.deleteParticipant(participant.email);
    server.close();
  });

  describe(`Without ${COIN_REWARD_PROPERTY}`, () => {
    missingTester.testProperty(COIN_REWARD_PROPERTY);
  });

  describe(`With invalid ${COIN_REWARD_PROPERTY}`, () => {
    invalidTester.testProperty(COIN_REWARD_PROPERTY, invalidInput.NEGATIVE_AMOUNT);
  });

  describe(`Without ${END_DATE_PROPERTY}`, () => {
    missingTester.testProperty(END_DATE_PROPERTY);
  });

  describe(`With invalid ${END_DATE_PROPERTY} date contains symbol`, () => {
    invalidTester.testProperty(END_DATE_PROPERTY, invalidInput.DATE_CONTAINS_SYMBOL);
  });

  describe(`With invalid ${END_DATE_PROPERTY} date in past`, () => {
    invalidTester.testProperty(END_DATE_PROPERTY, invalidInput.DATE_PAST);
  });

  describe(`Without ${PARTICIPANT_ID_PROPERTY}`, () => {
    missingTester.testProperty(PARTICIPANT_ID_PROPERTY);
  });

  describe(`With invalid ${PARTICIPANT_ID_PROPERTY}`, () => {
    invalidTester.testProperty(PARTICIPANT_ID_PROPERTY, invalidInput.ID_TOO_SHORT);
  });

  describe(`Without ${MEASUREMENTS_PROPERTY}`, () => {
    missingTester.testProperty(MEASUREMENTS_PROPERTY);
  });

  describe(`With invalid ${MEASUREMENTS_PROPERTY}`, () => {
    invalidTester.testProperty(MEASUREMENTS_PROPERTY, invalidInput.OBECT_IS_STRING);
  });

  describe(`Without ${MEASUREMENTS_PROPERTY} ${QUANITY_PROPERTY}`, () => {
    missingTester.testProperty(QUANITY_PROPERTY, deleteMeasurementPropertyFunc);
  });

  describe(`With invalid ${MEASUREMENTS_PROPERTY} ${QUANITY_PROPERTY}`, () => {
    invalidTester.testProperty(
      QUANITY_PROPERTY, invalidInput.NEGATIVE_AMOUNT, setMeasurementPropertyFunc,
    );
  });

  describe(`Without ${MEASUREMENTS_PROPERTY} ${UNIT_PROPERTY}`, () => {
    missingTester.testProperty(UNIT_PROPERTY, deleteMeasurementPropertyFunc);
  });

  describe(`With invalid ${MEASUREMENTS_PROPERTY} ${UNIT_PROPERTY}`, () => {
    invalidTester.testProperty(
      UNIT_PROPERTY, invalidInput.NAME_CONTAINS_SYMBOL, setMeasurementPropertyFunc,
    );
  });

  describe(`With a non existing ${PARTICIPANT_ID_PROPERTY}`, async () => {
    invalidTester.testProperty(PARTICIPANT_ID_PROPERTY, invalidInput.NON_EXISTING_ID);
  });
});
