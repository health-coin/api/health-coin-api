import { web3 } from '../../index';

const ID_REGEX = /^[a-fA-F0-9]{24}$/;
const ROLE_REGEX = /^([a-zA-Z]{1,30})$/;
const PATH_REGEX = /^([a-zA-Z/:]{1,})$/;
const METHODS = ['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE', 'PATCH'];

export const invalidAddress = (address: string) => {
  return !web3.utils.isAddress(address);
};

export const invalidId = (id: string) => {
  return !ID_REGEX.test(id);
};

export const invalidRole = (role: string) => {
  return !ROLE_REGEX.test(role);
};

export const invalidMethod = (method: string) => {
  return !METHODS.includes(method);
};

export const invalidPath = (path: string) => {
  return !PATH_REGEX.test(path);
};
