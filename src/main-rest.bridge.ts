import * as Restify from 'restify';
import * as MainController from './main.controller';
import * as MainRoute from './main.route';

export const loadServices = (server: Restify.Server) => {
  server.get({ path: '/user/balance' }, MainRoute.hasRights, MainRoute.getUserBalance);

  /**
   * Create a transaction from one user address to another.
   * @param id = Address from
   * @param to = Adress to
   */
  server.get({ path: '/users/:to/reward' }, async (req, res, next) => {
    try {
      res.send(await MainController.rewardHealthcoins(req.params));
      return next();
    } catch (err) {
      return next(err);
    }
  });

  /**
   * Used to print the total balance (stock) of healthcoins in the healthcoin contract.
   * Only for testing purposes!
   */
  server.get({ path: '/contract/balance' }, async (req, res, next) => {
    try {
      res.send(await MainController.getBalance(req.params));
      return next();
    } catch (err) {
      return next(err);
    }
  });

  server.post({ path: '/transactions' }, MainRoute.hasRights, MainRoute.createTransaction);

  server.post({ path: '/participants' }, MainRoute.createParticipant);
  server.get({ path: '/participants' }, MainRoute.hasRights, MainRoute.getAllParticipants);

  server.get(
    { path: '/participant/careprofessionals/:address' }
    , MainRoute.hasRights,
    MainRoute.getCareProvider,
  );
  server.get(
    { path: '/careprofessional/participants/:address' },
    MainRoute.hasRights,
    MainRoute.getParticipant,
  );
  server.get(
    { path: '/careprofessional/participants' },
    MainRoute.hasRights,
    MainRoute.getParticipants,
  );

  server.post({ path: '/login' }, MainRoute.loginUser);

  server.post({ path: '/treatmentplans' }, MainRoute.hasRights, MainRoute.createTreatmentplan);
  server.get({ path: '/treatmentplans' }, MainRoute.hasRights, MainRoute.findTreatmentPlans);
  server.get({ path: '/treatmentplans/:id' }, MainRoute.hasRights, MainRoute.findTreatmentplanById);
  server.get({ path: '/treatmentplans/active' },
    MainRoute.hasRights,
    MainRoute.findActiveTreatmentPlan,
  );
  /**
   * De logica is opgedeeld in drie gedeeltes om ieder de status aan te laten passen
   * - /sign = Status SIGNED
   * - /addFirstMeasurement = Status STARTED
   * - /addEndMeasurement = Stuats DONE
   */
  server.post(
    { path: '/treatmentplans/:id/sign' },
    MainRoute.hasRights, MainRoute.acceptTreatmentplan,
  );
  server.post(
    { path: '/treatmentplans/:id/measurements' },
    MainRoute.hasRights, MainRoute.addMeasurement,
  );
};
