FROM node:11

ARG SERVICE_PATH=/srv/healthcoin/healthcoin-api

# Create app directory
RUN mkdir -p $SERVICE_PATH
WORKDIR $SERVICE_PATH

# Place source code
COPY . $SERVICE_PATH

RUN npm install --production && \
    npm i typescript ts-node chai && \
    npm i -g mocha && \
    npm i -g nyc && \
    npm run clean && \
    npm run build

CMD node dist/src/index.js