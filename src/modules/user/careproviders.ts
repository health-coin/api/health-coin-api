import * as PersistenceUtils from '../../../test/tests/persistence-utils';
import * as TestUtils from '../../../test/tests/test-utils';
import validInput from '../../../test/tests/validation/valid/valid-input';

const FIRST_PROVIDER_EMAIL = 'careProf@test.com';
const SECOND_PROVIDER_EMAIL = 'careProf2@test.com';
const FIRST_POLICY_NUMBER = '12319231';
const SECOND_POLICY_NUMBER = '92312235';
const FIRST_ORGANISATION_NAME = 'Utrechtzorg';
const SECOND_ORGANISATION_NAME = 'ActiZ';

export const createOrFindCareProviders = async () => {
    const firstProvider = await createFirstProvider();
    const secondProvider = await createSecondProvider();
    return [firstProvider, secondProvider];
};

const createFirstProvider = async () => {
    const personProperties = TestUtils.createClone(validInput.PERSON_PROPERTIES);
    personProperties.policyNumber = FIRST_POLICY_NUMBER;
    const providerProperties = TestUtils.createClone(validInput.CARE_PROVIDER_REQUEST);
    providerProperties.email = FIRST_PROVIDER_EMAIL;
    return PersistenceUtils.createOrFindCareProvider(
        providerProperties, personProperties, FIRST_ORGANISATION_NAME,
    );
};

const createSecondProvider = async () => {
    const personProperties = TestUtils.createClone(validInput.PERSON_PROPERTIES);
    personProperties.policyNumber = SECOND_POLICY_NUMBER;
    const providerProperties = TestUtils.createClone(validInput.CARE_PROVIDER_REQUEST);
    providerProperties.email = SECOND_PROVIDER_EMAIL;
    return PersistenceUtils.createOrFindCareProvider(
        providerProperties, personProperties, SECOND_ORGANISATION_NAME,
    );
};
