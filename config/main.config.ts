interface Config {
  accessTokenKey: string;
  environment: string;
  ethereumHost: string;
  ethereumPort: number;
  port: number;
  refreshTokenKey: string;
  serverName: string;
  serverVersion: string;
  healthCoinAddress: string;
  healthCoinOwnerAddress: string;
  validTokenMinutes: number;
}

const main: Config = {
  accessTokenKey: process.env.ACCESS_TOKEN_KEY || 'LpMcxSPAUMkxNhxbvbKcZfuJ4NQ8wcp2',
  environment: process.env.ENVIRONMENT || 'test',
  ethereumHost: process.env.ETHEREUM_HOST || 'http://localhost',
  ethereumPort: parseInt(process.env.ETHEREUM_PORT, 10) || 7545,
  healthCoinAddress: process.env.HEALTH_COIN_ADDRESS
    || '0x61BA19b68314770e4bb46Bc41ce3E5Fa6BDF42a8',
  healthCoinOwnerAddress: process.env.HEALTH_COIN_OWNER_ADDRESS
    || '0xb87AcdfF00F0c31a01cF7Cc55a1acd037E772a31',
  port: parseInt(process.env.PORT, 10) || 8090,
  refreshTokenKey: process.env.REFRESH_TOKEN_KEY
    || 'Zv69pEzjntDPN8jcUdWQeKNKAnZsuVMaMUcD',
  serverName: process.env.SERVER_NAME || 'healthcoin-api',
  serverVersion: process.env.SERVER_VERSION || '1.0.0',
  validTokenMinutes: parseInt(process.env.VALID_TOKEN_MINUTES, 10) || 120,
};

export default main;
