import * as Chai from 'chai';
import 'mocha';
import * as Sinon from 'sinon';
import { server } from '../../../src/index';
import { CreateParticipantRequest } from '../../../src/modules/user/typings';
import * as UserController from '../../../src/modules/user/user.controller';
import * as UserRoute from '../../../src/modules/user/user.route';
import * as PersistenceUtils from '../persistence-utils';
import * as TestUtils from '../test-utils';
import InvalidPropertyTester from '../utils/InvalidPropertyTester';
import MissingPropertyTester from '../utils/MissingPropertyTester';
import * as ErrorChecks from '../validation/invalid/error-checks';
import invalidInput from '../validation/invalid/invalid-input';
import * as ValidBodyChecks from '../validation/valid/body-checks';
import validInput from '../validation/valid/valid-input';

const VALID_REQUEST: CreateParticipantRequest = TestUtils.createClone(
  validInput.PARTICIPANT_REQUEST,
);

const EMAIL_PROPERTY = 'email';
const PASSWORD_PROPERTY = 'password';
const PERSONAL_ID_PROPERTY = 'personalIdNumber';
const PERSON_PROPERTIES_PROPERTY = 'personProperties';

const DOB_PROPERTY = 'dateOfBirth';
const FIRST_NAME_PROPERTY = 'firstName';
const LAST_NAME_PROPERTY = 'lastName';
const GENDER_PROPERTY = 'gender';
const POLICY_NUMBER_PROPERTY = 'policyNumber';

const invalidTester = new InvalidPropertyTester(VALID_REQUEST, UserRoute.createParticipant);
const missingTester = new MissingPropertyTester(VALID_REQUEST, UserRoute.createParticipant);
const should = Chai.should();

const deletePersonalPropertyFunc = (validObject: any, propertyName: string): void => {
  delete validObject[PERSON_PROPERTIES_PROPERTY][propertyName];
};

const setPersonalPropertyFunc = (validObject: any, propertyName: string, property: any): void => {
  validObject[PERSON_PROPERTIES_PROPERTY][propertyName] = property;
};

describe('Create participant test', () => {
  before('Stub createEthereumAccount', () => {
    Sinon.stub(UserController, 'createEthereumAccount').callsFake(async (password: string) => {
      return validInput.BLOCKCHAIN_ADDRESS_ONE;
    });
  });

  after('Close server and restore', () => {
    Sinon.restore();
    server.close();
  });

  beforeEach('Make sure the test user doesn\'t exist', () => {
    PersistenceUtils.deleteParticipant(VALID_REQUEST.email);
  });
  afterEach('Make sure the test user doesn\'t exist', () => {
    PersistenceUtils.deleteParticipant(VALID_REQUEST.email);
  });

  describe(`Without ${EMAIL_PROPERTY}`, () => {
    missingTester.testProperty(EMAIL_PROPERTY);
  });

  describe(`With invalid ${EMAIL_PROPERTY}`, () => {
    invalidTester.testProperty(EMAIL_PROPERTY, invalidInput.EMAIL_NO_AT);
  });

  describe(`Without ${PASSWORD_PROPERTY}`, () => {
    missingTester.testProperty(PASSWORD_PROPERTY);
  });

  describe(`With invalid ${PASSWORD_PROPERTY}`, () => {
    invalidTester.testProperty(PASSWORD_PROPERTY, invalidInput.PASSWORD_CONTAINS_SPACES);
  });

  describe(`Without ${PERSONAL_ID_PROPERTY}`, () => {
    missingTester.testProperty(PERSONAL_ID_PROPERTY);
  });

  describe(`With invalid ${PERSONAL_ID_PROPERTY}`, () => {
    invalidTester.testProperty(PERSONAL_ID_PROPERTY, invalidInput.PERSONAL_ID_CONTAINS_LETTER);
  });

  describe(`Without ${DOB_PROPERTY}`, () => {
    missingTester.testProperty(DOB_PROPERTY, deletePersonalPropertyFunc);
  });

  describe(`With invalid ${DOB_PROPERTY}`, () => {
    invalidTester.testProperty(
      DOB_PROPERTY, invalidInput.DATE_CONTAINS_SYMBOL, setPersonalPropertyFunc,
    );
  });

  describe(`With ${DOB_PROPERTY} in the future`, () => {
    invalidTester.testProperty(
      DOB_PROPERTY, invalidInput.DATE_FUTURE, setPersonalPropertyFunc,
    );
  });

  describe(`Without ${FIRST_NAME_PROPERTY}`, () => {
    missingTester.testProperty(FIRST_NAME_PROPERTY, deletePersonalPropertyFunc);
  });

  describe(`With invalid ${FIRST_NAME_PROPERTY}`, () => {
    invalidTester.testProperty(
      FIRST_NAME_PROPERTY, invalidInput.NAME_CONTAINS_SYMBOL, setPersonalPropertyFunc,
    );
  });

  describe(`Without ${LAST_NAME_PROPERTY}`, () => {
    missingTester.testProperty(LAST_NAME_PROPERTY, deletePersonalPropertyFunc);
  });

  describe(`With invalid ${LAST_NAME_PROPERTY}`, () => {
    invalidTester.testProperty(
      LAST_NAME_PROPERTY, invalidInput.NAME_CONTAINS_SYMBOL, setPersonalPropertyFunc,
    );
  });

  describe(`Without ${GENDER_PROPERTY}`, () => {
    missingTester.testProperty(GENDER_PROPERTY, deletePersonalPropertyFunc);
  });

  describe(`With invalid ${GENDER_PROPERTY}`, () => {
    invalidTester.testProperty(
      GENDER_PROPERTY, invalidInput.GENDER_LETTER, setPersonalPropertyFunc,
    );
  });

  describe(`Without ${POLICY_NUMBER_PROPERTY}`, () => {
    missingTester.testProperty(POLICY_NUMBER_PROPERTY, deletePersonalPropertyFunc);
  });

  describe(`With invalid ${POLICY_NUMBER_PROPERTY}`, () => {
    invalidTester.testProperty(
      POLICY_NUMBER_PROPERTY, invalidInput.POLICY_NUMBER_CONTAINS_LETTER, setPersonalPropertyFunc,
    );
  });

  describe(`With invalid ${PERSON_PROPERTIES_PROPERTY}`, () => {
    invalidTester.testProperty(
      PERSON_PROPERTIES_PROPERTY, invalidInput.OBECT_IS_STRING,
    );
  });

  describe(`With the same ${EMAIL_PROPERTY}`, () => {
    it(TestUtils.BAD_REQUEST_MESSAGE, async () => {
      await UserRoute.createParticipant(TestUtils.createClone(VALID_REQUEST));

      const clone = TestUtils.createClone(VALID_REQUEST);
      clone[PERSON_PROPERTIES_PROPERTY] = TestUtils.createClone(VALID_REQUEST.personProperties);
      clone[PERSON_PROPERTIES_PROPERTY][POLICY_NUMBER_PROPERTY] = validInput.PERSONAL_ID_NUMBER2;
      clone[PERSONAL_ID_PROPERTY] = validInput.PERSONAL_ID_NUMBER2;

      try {
        const result2 = await UserRoute.createParticipant(clone);
        should.not.exist(result2);
      } catch (err) {
        ErrorChecks.validateBadRequest(err);
      }
    });
  });

  describe(`With the same ${PERSONAL_ID_PROPERTY}`, () => {
    it(TestUtils.BAD_REQUEST_MESSAGE, async () => {
      await UserRoute.createParticipant(TestUtils.createClone(VALID_REQUEST));

      const clone = TestUtils.createClone(VALID_REQUEST);
      clone[PERSON_PROPERTIES_PROPERTY] = TestUtils.createClone(VALID_REQUEST.personProperties);
      clone[PERSON_PROPERTIES_PROPERTY][POLICY_NUMBER_PROPERTY] = validInput.PERSONAL_ID_NUMBER2;
      clone[EMAIL_PROPERTY] = validInput.EMAIL;

      try {
        const result2 = await UserRoute.createParticipant(clone);
        should.not.exist(result2);
      } catch (err) {
        ErrorChecks.validateBadRequest(err);
      }
    });
  });

  describe(`With the same ${POLICY_NUMBER_PROPERTY}`, () => {
    it(TestUtils.BAD_REQUEST_MESSAGE, async () => {
      await UserRoute.createParticipant(TestUtils.createClone(VALID_REQUEST));

      const clone = TestUtils.createClone(VALID_REQUEST);
      clone[PERSON_PROPERTIES_PROPERTY] = TestUtils.createClone(VALID_REQUEST.personProperties);
      clone[PERSONAL_ID_PROPERTY] = validInput.PERSONAL_ID_NUMBER2;
      clone[EMAIL_PROPERTY] = validInput.EMAIL;

      try {
        const result2 = await UserRoute.createParticipant(clone);
        should.not.exist(result2);
      } catch (err) {
        ErrorChecks.validateBadRequest(err);
      }
    });
  });

  describe('With a valid request', () => {
    it('Should return a participant', async () => {
      const result = await UserRoute.createParticipant(VALID_REQUEST);
      ValidBodyChecks.validateSuccessResponse(result);
      ValidBodyChecks.validateParticipant(result.data);
    });
  });
});
