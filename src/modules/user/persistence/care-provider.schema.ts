import { Document, model, Model, Schema } from 'mongoose';
import Role from '../Role';
import accountSchema, { Account } from './account.schema';
import { Organisation, ORGANISATION_MODEL_NAME } from './organisation.schema';
import { Person, personProperties } from './person.schema';
import { Speciality, SPECIALITY_MODEL_NAME } from './speciality.schema';

const { ObjectId } = Schema.Types;
export const CAREPROVIDER_MODEL_NAME = Role.CareProvider;

const careProviderProperties = {
  organisation: {
    ref: ORGANISATION_MODEL_NAME,
    required: true,
    type: ObjectId,
  },
  phoneNumber: {
    required: true,
    type: String,
  },
  speciality: {
    ref: SPECIALITY_MODEL_NAME,
    required: true,
    type: ObjectId,
  },
};
const careProviderSchema = new Schema({ ...careProviderProperties, ...personProperties });

export interface CareProvider extends Account, Person {
  _id: string;
  organisation: Organisation;
  phoneNumber: string;
  speciality: Speciality;
}

type ModelType = CareProvider & Document;
let careProvider: Model<ModelType>;
try {
  careProvider = model<ModelType>(CAREPROVIDER_MODEL_NAME);
} catch (err) {
  accountSchema.discriminator(CAREPROVIDER_MODEL_NAME, careProviderSchema);
  careProvider = model<ModelType>(CAREPROVIDER_MODEL_NAME, careProviderSchema);
}
export default careProvider;
