import * as Mongoose from 'mongoose';
import { MongoConfig } from '../config/mongo.config';

export class MongoDB {

  public static getInstance(config: MongoConfig, logFunc: (error: any) => void): MongoDB {
    if (!this.instance) {
      this.instance = new MongoDB(config, logFunc);
    }
    return this.instance;
  }

  private static instance: MongoDB;
  private connection: Mongoose.Connection;

  private constructor(config: MongoConfig, logFunc: (error: any) => void) {

    Mongoose.connect(
      `mongodb://${config.mongoAddress}/${config.mongoDatabase}`, { useNewUrlParser: true },
    ).catch((error: any) => {
      logFunc(error);
      process.exit(-1);
    });

    this.connection = Mongoose.connection;

    this.connection.on('error', console.error.bind(console, 'connection error:'));
    this.connection.once('open', () => {
      logFunc('Succesfully connected to MongoDB');
    });
  }

  public getConnection(): Mongoose.Connection {
    return this.connection;
  }
}
